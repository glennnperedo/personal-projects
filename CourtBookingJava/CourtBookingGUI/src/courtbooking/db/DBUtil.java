/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courtbooking.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author siaykol
 */
public class DBUtil {

    private static Connection connection;

    private DBUtil() {
    }

    public static synchronized Connection getConnection() throws DBException {
        if (connection != null) {
            return connection;
        } else {
            try {
                // MySql set the db url, username, and password
//                String url = "jdbc:mysql://localhost:3306/courtreservation";
                String url = "jdbc:mysql://WIN-3PHV9FOPMK5:3306/courtreservation";
//                String username = "courtreservation_user";
                String username = "courtreservation_user";
                String password = "glenn";

                // get and return connection
                connection = DriverManager.getConnection(url, username, password);
                return connection;
            } catch (SQLException e) {
                throw new DBException(e);
            }
        }
    }

    public static synchronized void closeConnection() throws DBException {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DBException(e);
            } finally {
                connection = null;
            }
        }
    }

//    private Component frame;
//    private Connection conn;  
//      
//    public Connection GetConnection() {
//
//        try {    
//            String uHost = "jdbc:sqlserver://WIN-3PHV9FOPMK5:1433;DatabaseName=CourtReservationSQL";
//            String uName = "superadmin";
//            String uPass = "glenn";
//            conn = DriverManager.getConnection(uHost, uName, uPass);
//
////            JOptionPane.showMessageDialog(frame, "Connected...");
//
//       }
//        catch (SQLException err) {
//            JOptionPane.showMessageDialog(frame, err.getMessage());
//        }
//        finally {
//
//        }
//        return conn;
//    }
}
