/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courtbooking.db;

import courtbooking.ci.CourtInformation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.sql.Date;

/**
 *
 * @author siaykol
 */
public class BookingsDB {

    static boolean addOK = false;
    static boolean bookedOkay = false;

//    public void BookingsdDB() {
//        courtitems = null;
//    }
    public static List<CourtInformation> loadBookingInfo(LocalDate BookDate) throws DBException {
        int BookYear = BookDate.getYear();
        int BookMonth = BookDate.getMonthValue();
        int BookDay = BookDate.getDayOfMonth();
        List<CourtInformation> courtitems = new ArrayList<>();

        String sql = "SELECT * FROM tblBookings WHERE  "
                + "BookYear = ? AND BookMonth = ? AND BookDay = ? ";
        Connection connection = DBUtil.getConnection();

        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, BookYear);
            ps.setInt(2, BookMonth);
            ps.setInt(3, BookDay);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                CourtInformation ci = new CourtInformation();
                ci.setBookYear(rs.getInt("BookYear"));
                ci.setBookMonth(rs.getInt("BookMonth"));
                ci.setBookDay(rs.getInt("BookDay"));
                ci.setCourtNumber(rs.getInt("CourtNumber"));
                ci.setCourtTime(rs.getInt("CourtTime"));
                ci.setUserEmail(rs.getString("UserEmail"));
                ci.setStatus(rs.getInt("Status"));
                courtitems.add(ci);
            }
            return courtitems;

        } catch (SQLException e) {
            throw new DBException(e);
        }

    }

    public static List<CourtInformation> LoadUserBookingInfo(String logged_email) throws DBException {
        List<CourtInformation> userbookeditems = new ArrayList<>();
        String sql = "SELECT * FROM tblUserBookedCourts WHERE UserEmail = ? AND Status < ?";
        Connection connection = DBUtil.getConnection();

        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, logged_email);
            ps.setInt(2, 2);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                CourtInformation ci = new CourtInformation();
                //convert to localdate
                ci.setBookDate(rs.getDate("BookDate").toLocalDate());
                ci.setCourtNumber(rs.getInt("BookCourt"));
                ci.setCourtTime(rs.getInt("BookTime"));
                ci.setStatus(rs.getInt("Status"));
                userbookeditems.add(ci);
            }
//            if (userbookeditems.size() > 0) {
//                return userbookeditems;
//            } else
//                return null;
            return userbookeditems;
            
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    public static boolean AddNewBooking(CourtInformation ci) throws DBException {
        String sql = "INSERT INTO tblBookings (BookYear, BookMonth, BookDay, CourtNumber, CourtTime, UserEmail, Status) "
                + "VALUES (?,?,?,?,?,?,?)";
        Connection connection = DBUtil.getConnection();

        try (PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setInt(1, ci.getBookYear());
            ps.setInt(2, ci.getBookMonth());
            ps.setInt(3, ci.getBookDay());
            ps.setInt(4, ci.getCourtNumber());
            ps.setInt(5, ci.getCourtTime());
            ps.setString(6, ci.getUserEmail());
            ps.setInt(7, ci.getStatus());

            int recordAdded = ps.executeUpdate();
            addOK = recordAdded > 0;

        } catch (SQLException e) {
            throw new DBException(e);
        }

        return addOK;
    }

    public static boolean addUserBooking(CourtInformation ci) throws DBException {
        String sql = "INSERT INTO tblUserBookedCourts "
                + "(UserEmail, BookYear, BookMonth, BookDay, BookCourt, BookTime, BookDate, BookedDate, Status) "
                + "VALUES (?,?,?,?,?,?,?,?,?)";
        Connection connection = DBUtil.getConnection();

        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, ci.getUserEmail());
            ps.setInt(2, ci.getBookYear());
            ps.setInt(3, ci.getBookMonth());
            ps.setInt(4, ci.getBookDay());
            ps.setInt(5, ci.getCourtNumber());
            ps.setInt(6, ci.getCourtTime());
            //convert localdate to date
            LocalDate localdate = ci.getBookDate();
            //convert localdate to sql date
            Date sqlDate = Date.valueOf(localdate);
            ps.setDate(7, sqlDate);

            LocalDate today = LocalDate.now();
            //convert localdate to sql date
            Date bookeddate = Date.valueOf(today);
            ps.setDate(8, bookeddate);
            ps.setInt(9, ci.getStatus());

            int recordAdded = ps.executeUpdate();

            addOK = recordAdded > 0;

        } catch (SQLException e) {
            throw new DBException(e);
        }

        return addOK;
    }

    public static boolean CheckCourtAvailability(CourtInformation ci) throws DBException {
        String sql = "SELECT * FROM tblBookings WHERE  "
                + "BookYear = ? AND BookMonth = ? AND BookDay = ? AND CourtNumber = ? AND CourtTime =?";
        Connection connection = DBUtil.getConnection();

        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, ci.getBookYear());
            ps.setInt(2, ci.getBookMonth());
            ps.setInt(3, ci.getBookDay());
            ps.setInt(4, ci.getCourtNumber());
            ps.setInt(5, ci.getCourtTime());
            ResultSet rs = ps.executeQuery();

            bookedOkay = rs.next();     //replaced if (rs.next)

        } catch (SQLException e) {
            throw new DBException(e);
        }
        return bookedOkay;
    }

}
