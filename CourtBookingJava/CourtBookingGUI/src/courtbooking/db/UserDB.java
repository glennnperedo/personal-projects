/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courtbooking.db;

import courtbooking.ci.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author siaykol
 */
public class UserDB {

    public static UserInfo Login(String email, String password) throws DBException {
        UserInfo ui = new UserInfo();
        Connection connection = DBUtil.getConnection();
        String sql = "SELECT * FROM tblRegisteredUsers WHERE UserEmail = ? AND UserPassword = ?";

        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, email);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                ui.setEmail(rs.getString("UserEmail"));
                ui.setFirstName(rs.getString("UserFirstName"));
                ui.setLastName(rs.getString("UserLastName"));
                return ui;
            } else
                return null;

        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    public static boolean Register(UserInfo ci) throws DBException {
        boolean registerOK = false;

        String sql = "INSERT INTO tblRegisteredUsers (UserEmail, UserFirstName, UserLastName, "
                + "UserPassword, UserPhone, UserAddress)"
                + "VALUES (?,?,?,?,?,?)";
        Connection connection = DBUtil.getConnection();

        try (PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, ci.getEmail());
            ps.setString(2, ci.getFirstName());
            ps.setString(3, ci.getLastName());
            ps.setString(4, ci.getPassword());
            ps.setString(5, ci.getPhone());
            ps.setString(6, ci.getAddress());

            int recordAdded = ps.executeUpdate();

            //This line replaces the if statement below (compiler says redundant if statement)
            registerOK = recordAdded > 0;
//            if (recordAdded > 0) 
//                registerOK = true;
//            else
//                registerOK = false;
        } catch (SQLException e) {
            throw new DBException(e);
        }

        return registerOK;
    }
}
