/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courtbooking.ci;

import java.time.LocalDate;
import java.util.Date;
//import java.util.Date;

/**
 *
 * @author siaykol
 */
public class CourtInformation {

    private LocalDate BookDate;
    private int BookYear;
    private int BookMonth;
    private int BookDay;
    private int CourtNumber;
    private int CourtTime;
    private String UserEmail;
    private int Status;

    public CourtInformation() {
        BookDate = null;
        BookYear = 0;
        BookMonth = 0;
        BookDay = 0;
        CourtNumber = 0;
        CourtTime = 0;
        UserEmail = "";
        Status = 0;
    }

    public void setBookDate(LocalDate BookDate) {
        this.BookDate = BookDate;
    }

    public LocalDate getBookDate() {
        return BookDate;
    }

    public void setBookYear(int BookYear) {
        this.BookYear = BookYear;
    }

    public int getBookYear() {
        return BookYear;
    }

    public void setBookMonth(int BookMonth) {
        this.BookMonth = BookMonth;
    }

    public int getBookMonth() {
        return BookMonth;
    }

    public void setBookDay(int BookDay) {
        this.BookDay = BookDay;
    }

    public int getBookDay() {
        return BookDay;
    }

    public void setCourtTime(int CourtTime) {
        this.CourtTime = CourtTime;
    }

    public int getCourtTime() {
        return CourtTime;
    }

    public void setCourtNumber(int CourtNumber) {
        this.CourtNumber = CourtNumber;
    }

    public int getCourtNumber() {
        return CourtNumber;
    }

    public void setUserEmail(String UserEmail) {
        this.UserEmail = UserEmail;
    }

    public String getUserEmail() {
        return UserEmail;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public int getStatus() {
        return Status;
    }
}
