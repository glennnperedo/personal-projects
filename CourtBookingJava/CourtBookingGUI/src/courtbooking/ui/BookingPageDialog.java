package courtbooking.ui;

import courtbooking.ci.CourtInformation;
import courtbooking.db.BookingsDB;
import courtbooking.db.DBException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author siaykol
 */
public class BookingPageDialog extends JDialog {

    private JButton date1Button;
    private JButton date2Button;
    private JButton date3Button;
    private JButton date4Button;
    private JButton date5Button;
    private JButton date6Button;
    private JLabel textLabel;
    private JLabel textLabel2;
    private JTable BookingPageTable;
    private int clickedButton;
    private final String email;

    public BookingPageDialog(java.awt.Frame parent, String title, boolean modal, String email) {
        super(parent, title, modal);
        this.setDefaultCloseOperation((WindowConstants.DISPOSE_ON_CLOSE));

        this.email = email;
        this.clickedButton = 1;
        initComponents();

    }

    private void initComponents() {

        //Text label
        textLabel = new JLabel("Filipino Calgary Tennis Club");
        textLabel.setFont(new java.awt.Font("Tahoma", 1, 16));
        textLabel.setForeground(Color.BLUE);
//        textLabel.setPreferredSize(new Dimension(400, 30));
//        textLabel.setMinimumSize(new Dimension(100, 30));
        textLabel.setHorizontalAlignment(SwingConstants.CENTER);

        textLabel2 = new JLabel("Booking Page");
        textLabel2.setFont(new java.awt.Font("Tahoma", 1, 14));
        textLabel2.setForeground(Color.DARK_GRAY);
//        textLabel2.setPreferredSize(new Dimension(100, 20));
//        textLabel2.setMinimumSize(new Dimension(100, 50));
        textLabel2.setHorizontalAlignment(SwingConstants.CENTER);

        //Text Panel
        JPanel titlepanel = new JPanel(new BorderLayout());
        titlepanel.setPreferredSize(new Dimension(600, 80));

        JPanel panel = new JPanel(new BorderLayout());
        panel.setPreferredSize(new Dimension(300, 40));
        panel.add(textLabel, BorderLayout.CENTER);
        titlepanel.add(panel, BorderLayout.PAGE_START);

        JPanel panel2 = new JPanel(new BorderLayout());
        panel2.add(textLabel2, BorderLayout.NORTH);
        titlepanel.add(panel2, BorderLayout.CENTER);

        //add panels to main  
        setPreferredSize(new Dimension(900, 1020));
        setLayout(new FlowLayout());
        add(titlepanel);
        add(buildButtonPanel(), BorderLayout.SOUTH);

        //Add Scroll Pane with the Bookingpage table
        add(buildBookingTable(), BorderLayout.CENTER);

        populateBookingTable(LocalDate.now());

        pack();

    }

    private JScrollPane buildBookingTable() {

        //Initialize the arrays
        String[] header = new String[]{
            "Time", "Court 1", "Court 2", "Court 3", "Court 4"
        };

        //actual data for the table in a 2d array
        String[][] rows = new String[][]{
            {"07 AM", null, null, null, null},
            {"08 AM", null, null, null, null},
            {"09 AM", null, null, null, null},
            {"10 AM", null, null, null, null},
            {"11 AM", null, null, null, null},
            {"12 PM", null, null, null, null},
            {"01 PM", null, null, null, null},
            {"02 PM", null, null, null, null},
            {"03 PM", null, null, null, null},
            {"04 PM", null, null, null, null},
            {"05 PM", null, null, null, null},
            {"06 PM", null, null, null, null},
            {"07 PM", null, null, null, null}
        };

        //Setup the table and return in a JSrcollpane
        BookingPageTable = new JTable();
        DefaultTableModel dtm = new DefaultTableModel(rows, header);
        BookingPageTable.setModel(dtm);
        BookingPageTable.getTableHeader().setDefaultRenderer(new SimpleHeaderRenderer());   //set header style
        BookingPageTable.setCellSelectionEnabled(true);
        BookingPageTable.setPreferredSize(new Dimension(800, 780));

        BookingPageTable.setRowHeight(60);
        BookingPageTable.getTableHeader().setPreferredSize(new Dimension(100, 40));

        BookingPageTable.setShowVerticalLines(rootPaneCheckingEnabled);
        BookingPageTable.setShowHorizontalLines(rootPaneCheckingEnabled);
        BookingPageTable.setGridColor(Color.lightGray);

        SetCellsAlignment(BookingPageTable, SwingConstants.CENTER);
//        SetHeadersAlignment(BookingPageTable, SwingConstants.CENTER);     //!!!!!!!!This is perventing the header properties on font color. WHY?

        //Listener for Mouse Click (Anonymous Class)
        BookingPageTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                int clickedRow = BookingPageTable.getSelectedRow();
                int clickedColumn = BookingPageTable.getSelectedColumn();

                if (clickedColumn == 0) {
                    return;
                }
                JDialog.setDefaultLookAndFeelDecorated(true);
                int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to book this court?", "Confirm",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (response == JOptionPane.YES_OPTION) {
                    addBookedCourt(clickedRow, clickedColumn);
                }
            }
        });  //end of mouse listener

        JScrollPane tableScrollPane = new JScrollPane();
        tableScrollPane.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));
        tableScrollPane.setPreferredSize(new Dimension(800, 822));
        tableScrollPane.setViewportView(BookingPageTable);
        return tableScrollPane;

    }

    public void addBookedCourt(int row, int col) {

        //buttonactive determines how many days to add from today (ie if button 6 is clicked, add 5 days
        int activebutton = clickedButton - 1;

        //Use LocalDate
        LocalDate BookDate = LocalDate.now();
        LocalDate localDate = BookDate.plusDays(activebutton);

        CourtInformation ci = new CourtInformation();
        ci.setBookDate(localDate);
        ci.setBookYear(localDate.getYear());
        ci.setBookMonth(localDate.getMonthValue());
        ci.setBookDay(localDate.getDayOfMonth());
        ci.setCourtNumber(col);
        ci.setCourtTime(row);
        ci.setUserEmail(email);
        ci.setStatus(0);

        try {
            if (BookingsDB.CheckCourtAvailability(ci)) {    //court is available
                JOptionPane.showMessageDialog(this, "Court is already booked!");
                return;
            }
        } catch (DBException ex) {
            JOptionPane.showMessageDialog(this, ex);
        }

        try {
            if (BookingsDB.AddNewBooking(ci)) {          //Court has been booked,  create user booked coourt
                ci.setBookDate(BookDate);

                try {
                    //Add user booking record
                    if (!BookingsDB.addUserBooking(ci)) {
                        JOptionPane.showMessageDialog(this, "Problem creating user booked item.");
                    } else {
                        populateBookingTable(localDate);
                    }
                } catch (DBException ex) {
                    JOptionPane.showMessageDialog(this, ex);
                }
            }
        } catch (DBException ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
    }

    public static void SetCellsAlignment(JTable table, int alignment) {
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(alignment);

        TableModel tableModel = table.getModel();

        for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++) {
            table.getColumnModel().getColumn(columnIndex).setCellRenderer(renderer);
        }
    }

    public static void SetHeadersAlignment(JTable table, int alignment) {
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(alignment);

        TableModel tableModel = table.getModel();

        for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++) {
            table.getColumnModel().getColumn(columnIndex).setHeaderRenderer(renderer);
        }
    }

    private JPanel buildButtonPanel() {
        LocalDate nextday;
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("E MMM dd, yyyy");
        String dateformatted;

        JPanel panel = new JPanel();

        dateformatted = dtf.format(currentDate);
        date1Button = new JButton(dateformatted);
        date1Button.setName("1");
        date1Button.addActionListener((ActionEvent) -> {
            setBookDateButtons(date1Button);
        });

        nextday = currentDate.plusDays(1);
        dateformatted = dtf.format(nextday);
        date2Button = new JButton(dateformatted);
        date2Button.setName("2");
        date2Button.addActionListener((ActionEvent) -> {
            setBookDateButtons(date2Button);
        });

        nextday = currentDate.plusDays(2);
        dateformatted = dtf.format(nextday);
        date3Button = new JButton(dateformatted);
        date3Button.setName("3");
        date3Button.addActionListener((ActionEvent) -> {
            setBookDateButtons(date3Button);
        });

        nextday = currentDate.plusDays(3);
        dateformatted = dtf.format(nextday);
        date4Button = new JButton(dateformatted);
        date4Button.setName("4");
        date4Button.addActionListener((ActionEvent) -> {
            setBookDateButtons(date4Button);
        });

        nextday = currentDate.plusDays(4);
        dateformatted = dtf.format(nextday);
        date5Button = new JButton(dateformatted);
        date5Button.setName("5");
        date5Button.addActionListener((ActionEvent) -> {
            setBookDateButtons(date5Button);
        });

        nextday = currentDate.plusDays(5);
        dateformatted = dtf.format(nextday);
        date6Button = new JButton(dateformatted);
        date6Button.setName("6");
        date6Button.addActionListener((ActionEvent) -> {
            setBookDateButtons(date6Button);
        });

        panel.setLayout(new FlowLayout(FlowLayout.CENTER));
        panel.add(date1Button);
        panel.add(date2Button);
        panel.add(date3Button);
        panel.add(date4Button);
        panel.add(date5Button);
        panel.add(date6Button);

        //return created panel with buttons
        return panel;
    }

    public void setBookDateButtons(JButton button) {
        LocalDate bookDate = LocalDate.now();
        LocalDate currentDate;

        setButtonColor();

        switch (button.getName()) {
            case "1":
                date1Button.setForeground(Color.BLUE);
                clickedButton = 1;
                break;
            case "2":
                date2Button.setForeground(Color.BLUE);
                clickedButton = 2;
                currentDate = LocalDate.now();
                bookDate = currentDate.plusDays(1);
                break;
            case "3":
                date3Button.setForeground(Color.BLUE);
                clickedButton = 3;
                currentDate = LocalDate.now();
                bookDate = currentDate.plusDays(2);
                break;
            case "4":
                date4Button.setForeground(Color.BLUE);
                clickedButton = 4;
                currentDate = LocalDate.now();
                bookDate = currentDate.plusDays(3);
                break;
            case "5":
                date5Button.setForeground(Color.BLUE);
                clickedButton = 5;
                currentDate = LocalDate.now();
                bookDate = currentDate.plusDays(4);
                break;
            case "6":
                date6Button.setForeground(Color.BLUE);
                clickedButton = 6;
                currentDate = LocalDate.now();
                bookDate = currentDate.plusDays(5);
                break;
        }
        populateBookingTable(bookDate);
    }

    private void populateBookingTable(LocalDate bookDate) {
        //Clear Table Layout cells
        String booked = "";
        List<CourtInformation> courtitems = new ArrayList<>();

        for (int i = 0; i < 13; i++) {              //row
            for (int j = 1; j < 5; j++) {           //column starts at 1
                BookingPageTable.setValueAt(booked, i, j);
            }
        }

        try {
            courtitems = BookingsDB.loadBookingInfo(bookDate);
        } catch (DBException e) {
            JOptionPane.showMessageDialog(this, e);
        }

        int row = 0;
        for (CourtInformation ci : courtitems) {
            //time is used as index to gridview row
            int time = ci.getCourtTime();

            //Convert time to timeAM/PM
            switch (time) {
                case 0:
                    booked = "Booked " + "\n" + "07:00 AM";
                    break;
                case 1:
                    booked = "Booked " + "\n" + "08:00 AM";
                    break;
                case 2:
                    booked = "Booked " + "\n" + "09:00 AM";
                    break;
                case 3:
                    booked = "Booked " + "\n" + "10:00 AM";
                    break;
                case 4:
                    booked = "Booked " + "\n" + "11:00 AM";
                    break;
                case 5:
                    booked = "Booked " + "\n" + "12:00 PM";
                    break;
                case 6:
                    booked = "Booked " + "\n" + "01:00 PM";
                    break;
                case 7:
                    booked = "Booked " + "\n" + "02:00 PM";
                    break;
                case 8:
                    booked = "Booked " + "\n" + "03:00 PM";
                    break;
                case 9:
                    booked = "Booked " + "\n" + "04:00 PM";
                    break;
                case 10:
                    booked = "Booked " + "\n" + "05:00 PM";
                    break;
                case 11:
                    booked = "Booked " + "\n" + "06:00 PM";
                    break;
                case 12:
                    booked = "Booked " + "\n" + "07:00 PM";
                    break;
            }

            if (ci.getStatus() == 1) {
                booked = booked + "\n Paid";
            }

            switch (ci.getCourtNumber()) {
                case 1:
                    BookingPageTable.setValueAt(booked, time, 1);   //params - String, row, col
                    BookingPageTable.getColumnModel().getColumn(1).setCellRenderer(new SimpleHeaderRenderer());

                    break;
                case 2:
                    BookingPageTable.setValueAt(booked, time, 2);
                    break;
                case 3:
                    BookingPageTable.setValueAt(booked, time, 3);
                    break;
                case 4:
                    BookingPageTable.setValueAt(booked, time, 4);
                    break;
            }
            row++;
        }

    }

    private void setButtonColor() {
        date1Button.setForeground(Color.black);
        date2Button.setForeground(Color.black);
        date3Button.setForeground(Color.black);
        date4Button.setForeground(Color.black);
        date5Button.setForeground(Color.black);
        date6Button.setForeground(Color.black);
    }
}
