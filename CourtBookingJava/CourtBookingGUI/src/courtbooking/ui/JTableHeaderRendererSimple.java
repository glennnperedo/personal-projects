/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courtbooking.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

/**
 * A Swing program demonstrates how to use a custom renderer for all column
 * headers of a JTable component.
 *
 * @author www.codejava.net
 *
 */
public class JTableHeaderRendererSimple extends JFrame {

    private JTable table;

    public JTableHeaderRendererSimple() {
        String[] header = new String[]{
            "Time", "Court 1", "Court 2", "Court 3", "Court 4"
        };

        //actual data for the table in a 2d array
        String[][] rows = new String[][]{
            {"07 AM", null, null, null, null},
            {"08 AM", null, null, null, null},
            {"09 AM", null, null, null, null},
            {"10 AM", null, null, null, null},
            {"11 AM", null, null, null, null},
            {"12 PM", null, null, null, null},
            {"01 PM", null, null, null, null},
            {"02 PM", null, null, null, null},
            {"03 PM", null, null, null, null},
            {"04 PM", null, null, null, null},
            {"05 PM", null, null, null, null},
            {"06 PM", null, null, null, null},
            {"07 PM", null, null, null, null}
        };
        setPreferredSize(new Dimension(900, 970));
        setLayout(new FlowLayout());
        
        JTable BookingPageTable = new JTable(rows, header);
        BookingPageTable.getTableHeader().setDefaultRenderer(new SimpleHeaderRenderer());
//        JScrollPane scrollpane = new JScrollPane();
//        add(scrollpane.add(BookingPageTable));

        add(new JScrollPane(BookingPageTable));
        BookingPageTable.setCellSelectionEnabled(true);
        BookingPageTable.setPreferredSize(new Dimension(800, 780));
        BookingPageTable.setRowHeight(60);
        BookingPageTable.getTableHeader().setPreferredSize(new Dimension(100, 40));

//        BookingPageTable.getTableHeader().setOpaque(false);
//        BookingPageTable.getTableHeader().setBackground(Color.CYAN);
//        BookingPageTable.getTableHeader().setForeground(Color.RED);
        BookingPageTable.setShowVerticalLines(rootPaneCheckingEnabled);
        BookingPageTable.setShowHorizontalLines(rootPaneCheckingEnabled);
        BookingPageTable.setGridColor(Color.lightGray);

//        super("JTable Column Header Custom Renderer Basic");
//         
//        // constructs the table
//        String[] columnNames = new String[] {"Title", "Author", "Publisher", "Published Date", "Pages", "Rating"};
//        String[][] rowData = new String[][] {
//            {"Effective Java", "Joshua Bloch", "Addision-Wesley", "May 08th 2008", "346", "5"},
//            {"Thinking in Java", "Bruce Eckel", "Prentice Hall", "Feb 26th 2006", "1150", "4"},
//            {"Head First Java", "Kathy Sierra & Bert Bates", "O'Reilly Media", "Feb 09th 2005", "688", "4.5"}, 
//        };
//                 
//        table = new JTable(rowData, columnNames);
//        table.getTableHeader().setDefaultRenderer(new SimpleHeaderRenderer());
//         
//        add(new JScrollPane(table));
//         
//        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        setSize(640, 150);
//        setLocationRelativeTo(null);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new JTableHeaderRendererSimple().setVisible(true);
            }
        });
    }
}
