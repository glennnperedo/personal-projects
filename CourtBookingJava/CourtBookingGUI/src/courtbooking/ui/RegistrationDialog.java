/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courtbooking.ui;

import com.mysql.jdbc.StringUtils;
import courtbooking.ci.UserInfo;
import courtbooking.db.DBException;
import courtbooking.db.UserDB;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 *
 * @author siaykol
 */
public class RegistrationDialog extends JDialog {

    private JTextField firstField;
    private JTextField lastField;
    private JTextField addressField;
    private JTextField phoneField;
    private JTextField emailField;
    private JPasswordField passwordField;
    private JButton saveButton;
    private JButton cancelButton;
    private JLabel textLabel;

    public RegistrationDialog(java.awt.Frame parent, String title, boolean modal) {
        super(parent, title, modal);
        this.setDefaultCloseOperation((WindowConstants.DISPOSE_ON_CLOSE));
        initComponents();
    }

    private void initComponents() {
        //create components
        firstField = new JTextField();
        lastField = new JTextField();
        addressField = new JTextField();
        phoneField = new JTextField();
        emailField = new JTextField();
        passwordField = new JPasswordField();
        saveButton = new JButton("Save");
        cancelButton = new JButton("Cancel");

        textLabel = new JLabel("User Registration Form");
        textLabel.setFont(new java.awt.Font("Tahoma", 1, 12));
        textLabel.setPreferredSize(new Dimension(200, 30));
        textLabel.setMinimumSize(new Dimension(200, 30));
        textLabel.setHorizontalAlignment(SwingConstants.CENTER);

        //set component size
        Dimension longField = new Dimension(300, 25);
        firstField.setPreferredSize(longField);
        firstField.setMinimumSize(longField);
        lastField.setPreferredSize(longField);
        lastField.setMinimumSize(longField);
        addressField.setPreferredSize(longField);
        addressField.setMinimumSize(longField);
        phoneField.setPreferredSize(longField);
        phoneField.setMinimumSize(longField);
        emailField.setPreferredSize(longField);
        emailField.setMinimumSize(longField);
        passwordField.setPreferredSize(longField);
        passwordField.setMinimumSize(longField);

        //Text Panel
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        textPanel.add(textLabel);

        // JLabel and JTextField panel
        JPanel userinfoPanel = new JPanel();
        userinfoPanel.setLayout(new GridBagLayout());
        userinfoPanel.add(new JLabel("First Name"), getConstraints(0, 0, GridBagConstraints.LINE_END));
        userinfoPanel.add(firstField, getConstraints(1, 0, GridBagConstraints.LINE_START));
        userinfoPanel.add(new JLabel("Last Name"), getConstraints(0, 1, GridBagConstraints.LINE_END));
        userinfoPanel.add(lastField, getConstraints(1, 1, GridBagConstraints.LINE_START));
        userinfoPanel.add(new JLabel("Address"), getConstraints(0, 2, GridBagConstraints.LINE_END));
        userinfoPanel.add(addressField, getConstraints(1, 2, GridBagConstraints.LINE_START));
        userinfoPanel.add(new JLabel("Phone"), getConstraints(0, 3, GridBagConstraints.LINE_END));
        userinfoPanel.add(phoneField, getConstraints(1, 3, GridBagConstraints.LINE_START));
        userinfoPanel.add(new JLabel("Email"), getConstraints(0, 4, GridBagConstraints.LINE_END));
        userinfoPanel.add(emailField, getConstraints(1, 4, GridBagConstraints.LINE_START));
        userinfoPanel.add(new JLabel("Password"), getConstraints(0, 5, GridBagConstraints.LINE_END));
        userinfoPanel.add(passwordField, getConstraints(1, 5, GridBagConstraints.LINE_START));

        //JButton Panel
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        buttonPanel.add(saveButton);
        buttonPanel.add(cancelButton);

        //add panels to main panel
        setLayout(new BorderLayout());
        add(textPanel, BorderLayout.NORTH);
        add(userinfoPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
        pack();

        saveButton.addActionListener((ActionEvent) -> {
            saveButtonClicked();
        });

        cancelButton.addActionListener((ActionEvent) -> {
            cancelButtonClicked();
        });
    }

    // Helper method to return GridBagConstraints objects
    private GridBagConstraints getConstraints(int x, int y, int anchor) {
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 0, 5);
        c.gridx = x;
        c.gridy = y;
        c.anchor = anchor;
        return c;
    }

    private void saveButtonClicked() {
        if (ValidateData()) {

            UserInfo ui = new UserInfo();

            ui.setEmail(emailField.getText());
            ui.setFirstName(firstField.getText());
            ui.setLastName(lastField.getText());
            ui.setAddress(addressField.getText());
            ui.setPhone(phoneField.getText());
            char[] passchar = passwordField.getPassword();
            ui.setPassword(String.valueOf(passchar));

            try {
                if (!UserDB.Register(ui)) {
                    JOptionPane.showMessageDialog(this, "Registration failed.", "User Registration", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, "You may now login.", "User Login", JOptionPane.ERROR_MESSAGE);
                }
                this.dispose(); //Close window
                this.setVisible(false); //Hide window
            } catch (DBException e) {
                JOptionPane.showMessageDialog(this, e);
            }

        } else {
            JOptionPane.showMessageDialog(this, "All fields are required and may not contain spaces only.", "Validate Data", JOptionPane.ERROR_MESSAGE);
        }

    }

    private void cancelButtonClicked() {
        this.dispose(); //Close window
//        this.setVisible(false); //Hide window
    }

    public boolean ValidateData() {
        char[] passchar = passwordField.getPassword();
        String password = String.valueOf(passchar);
        String email = emailField.getText();
        String firstname = firstField.getText();
        String lastname = lastField.getText();
        String address = addressField.getText();
        String phone = phoneField.getText();

        //replace if below with this line
//        return !(email.isEmpty() || firstname.isEmpty() || lastname.isEmpty() || address.isEmpty() || phone.isEmpty() || password.isEmpty()
//                ||  StringUtils.isEmptyOrWhitespaceOnly(email));
        return !(StringUtils.isEmptyOrWhitespaceOnly(email)
                || StringUtils.isEmptyOrWhitespaceOnly(firstname)
                || StringUtils.isEmptyOrWhitespaceOnly(lastname)
                || StringUtils.isEmptyOrWhitespaceOnly(address)
                || StringUtils.isEmptyOrWhitespaceOnly(phone)
                || StringUtils.isEmptyOrWhitespaceOnly(password));
    }
}
