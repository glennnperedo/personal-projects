/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courtbooking.ui;

import courtbooking.ci.CourtInformation;
import courtbooking.ci.UserInfo;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author Glenn P
 */
public class UserCourtsDialog extends JDialog {

    private JLabel usernameLabel;
    private JLabel titleLabel;
    private final List<CourtInformation> userbookedcourts;

    public UserCourtsDialog(java.awt.Frame parent, String title, boolean modal, List<CourtInformation> userbookedcourts, UserInfo ui) {
        super(parent, title, modal);
        this.setDefaultCloseOperation((WindowConstants.DISPOSE_ON_CLOSE));

        this.userbookedcourts = userbookedcourts;

        initComponents(ui);

    }

    private void initComponents(UserInfo ui) {
        //Text label
        titleLabel = new JLabel("Filipino Calgary Tennis Club");
        titleLabel.setFont(new java.awt.Font("Tahoma", 1, 14));
        titleLabel.setForeground(Color.BLUE);
        titleLabel.setHorizontalAlignment(SwingConstants.CENTER);

        usernameLabel = new JLabel(ui.getFirstName() + " " + ui.getLastName());
        usernameLabel.setFont(new java.awt.Font("Tahoma", 1, 12));
        usernameLabel.setForeground(Color.DARK_GRAY);
        usernameLabel.setHorizontalAlignment(SwingConstants.CENTER);

        //Text Panel
        JPanel titlepanel = new JPanel(new BorderLayout());
        titlepanel.setPreferredSize(new Dimension(500, 60));

        JPanel panel = new JPanel(new BorderLayout());
        panel.setPreferredSize(new Dimension(500, 40));
        panel.add(titleLabel, BorderLayout.CENTER);
        titlepanel.add(panel, BorderLayout.PAGE_START);

        JPanel panel2 = new JPanel(new BorderLayout());
        panel2.add(usernameLabel, BorderLayout.NORTH);
        titlepanel.add(panel2, BorderLayout.CENTER);

        //Main Frame
        //set size and add panels
        setPreferredSize(new Dimension(500, 520));
        setLayout(new FlowLayout());
        add(titlepanel);
        //Add Scroll Pane with the Bookingpage table
        add(buildUserBookedCourtsTable(), BorderLayout.CENTER);
        pack();
    }

    private JScrollPane buildUserBookedCourtsTable() {

        String[] header = new String[]{
            "Date", "Time", "Court", "Pay", "Cancel"
        };

        //actual data for the table in a 2d array.  Size is set for row and columns
        Object[][] rows = new Object[userbookedcourts.size()][5];

        JTable BookingPageTable;
        int i = 0;
        String time = "";

        for (CourtInformation court : userbookedcourts) {

            //format date before displaying
            LocalDate bookdate = court.getBookDate();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("E MMM dd, yyyy");
            String dateformatted = dtf.format(bookdate);

            switch (court.getCourtTime()) {
                case 0:
                    time = "07:00 AM";
                    break;
                case 1:
                    time = "08:00 AM";
                    break;
                case 2:
                    time = "09:00 AM";
                    break;
                case 3:
                    time = "10:00 AM";
                    break;
                case 4:
                    time = "11:00 AM";
                    break;
                case 5:
                    time = "12:00 PM";
                    break;
                case 6:
                    time = "01:00 PM";
                    break;
                case 7:
                    time = "02:00 PM";
                    break;
                case 8:
                    time = "03:00 PM";
                    break;
                case 9:
                    time = "04:00 PM";
                    break;
                case 10:
                    time = "05:00 PM";
                    break;
                case 11:
                    time = "06:00 PM";
                    break;
                case 12:
                    time = "07:00 PM";
                    break;
            }

            rows[i][0] = dateformatted;
            rows[i][1] = time;
            rows[i][2] = String.valueOf(court.getCourtNumber());
            rows[i][3] = "PAY";
            rows[i][4] = "CANCEL";

            i++;
        }

        BookingPageTable = new JTable();
        DefaultTableModel dtm = new DefaultTableModel(rows, header);
        BookingPageTable.setModel(dtm);
        BookingPageTable.getTableHeader().setDefaultRenderer(new SimpleHeaderRenderer());

        BookingPageTable.setCellSelectionEnabled(true);
        BookingPageTable.setPreferredSize(new Dimension(400, 500));

        BookingPageTable.setRowHeight(40);
        BookingPageTable.getTableHeader().setPreferredSize(new Dimension(400, 30));

        BookingPageTable.setShowVerticalLines(rootPaneCheckingEnabled);
        BookingPageTable.setShowHorizontalLines(rootPaneCheckingEnabled);
        BookingPageTable.setGridColor(Color.lightGray);

        //set column width
        TableColumn col0 = BookingPageTable.getColumnModel().getColumn(0);
        col0.setPreferredWidth(110);
        TableColumn col1 = BookingPageTable.getColumnModel().getColumn(1);
        col1.setPreferredWidth(40);
        TableColumn col2 = BookingPageTable.getColumnModel().getColumn(2);
        col2.setPreferredWidth(30);
        TableColumn col3 = BookingPageTable.getColumnModel().getColumn(3);
        col3.setPreferredWidth(30);
        TableColumn col4 = BookingPageTable.getColumnModel().getColumn(4);
        col4.setPreferredWidth(40);

        //Listener for Mouse Click (Anonymous Class)
        BookingPageTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                int clickedRow = BookingPageTable.getSelectedRow();
                int clickedColumn = BookingPageTable.getSelectedColumn();

                if (clickedColumn != -1) {

                    JDialog.setDefaultLookAndFeelDecorated(true);

                    if (clickedColumn == 4) {
                        int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to CANCEL this court?", "Confirm",
                                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                        if (response == JOptionPane.YES_OPTION) {

                            cancelCourt(clickedRow);
                        }
                    }

                    if (clickedColumn == 3) {
                        payCourt(clickedRow);
                    }
                }
            }
        });  //end of mouse listener

        JScrollPane tableScrollPane = new JScrollPane();
        tableScrollPane.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));
        tableScrollPane.setPreferredSize(new Dimension(400, 400));
        tableScrollPane.setViewportView(BookingPageTable);
        return tableScrollPane;

//        return BookingPageTable;
    }

    private void cancelCourt(int clickedRow) {
        CourtInformation ci = userbookedcourts.get(clickedRow);
//        System.out.println(ci.getBookDate());
//        System.out.println(ci.getCourtTime());
//        System.out.println(ci.getCourtNumber());
    }

    private void payCourt(int clickedRow) {
                CourtInformation ci = userbookedcourts.get(clickedRow);
        JOptionPane.showMessageDialog(null, "Court has been paid.", "Pay booked court", JOptionPane.INFORMATION_MESSAGE);
    }

//    public static void SetCellsAlignment(JTable table, int alignment) {
//        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
//        renderer.setHorizontalAlignment(alignment);
//
//        TableModel tableModel = table.getModel();
//
//        for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++) {
//            table.getColumnModel().getColumn(columnIndex).setCellRenderer(renderer);
//        }
//    }
//
//    public static void SetHeadersAlignment(JTable table, int alignment) {
//        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
//        renderer.setHorizontalAlignment(alignment);
//
//        TableModel tableModel = table.getModel();
//
//        for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++) {
//            table.getColumnModel().getColumn(columnIndex).setHeaderRenderer(renderer);
//        }
//    }
}
