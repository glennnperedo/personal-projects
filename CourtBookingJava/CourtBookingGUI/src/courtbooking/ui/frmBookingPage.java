/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package courtbooking.ui;

import courtbooking.ci.CourtInformation;
import courtbooking.db.BookingsDB;
import courtbooking.db.DBException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import javax.swing.JDialog;

/**
 *
 * @author siaykol
 */
public class frmBookingPage extends JFrame {

    List<CourtInformation> courtitems = new ArrayList<>();
    public String logged_email;
    public String logged_firstname;
    public String logged_lastname;
    Component frame = null;

    int clickedbutton;

    /**
     * Creates new form frmBookingPage
     */
    public frmBookingPage() {
        initComponents();

        SetBookingTable();
        SetBookingDates();
        clickedbutton = 1;
        SetButtonColor();
        btnDate1.setForeground(Color.BLUE);

        LocalDate dateToday = LocalDate.now();
        PopulateBookingItems(dateToday);
    }

    private void SetBookingDates() {
        LocalDate nextday;
        //JAVA 8 Date Time
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("E MMM dd yyyy");

        String dateformatted = dtf.format(currentDate);
        btnDate1.setText(dateformatted);
        nextday = currentDate.plusDays(1);
        dateformatted = dtf.format(nextday);
        btnDate2.setText(dateformatted);
        nextday = currentDate.plusDays(2);
        dateformatted = dtf.format(nextday);
        btnDate3.setText(dateformatted);
        nextday = currentDate.plusDays(3);
        dateformatted = dtf.format(nextday);
        btnDate4.setText(dateformatted);
        nextday = currentDate.plusDays(4);
        dateformatted = dtf.format(nextday);
        btnDate5.setText(dateformatted);
        nextday = currentDate.plusDays(5);
        dateformatted = dtf.format(nextday);
        btnDate6.setText(dateformatted);

        //Before JAVA 8 Date Time
        //        Date today = new Date();
        //        SimpleDateFormat simpleDateformat = new SimpleDateFormat("E MMM dd, yyyy"); // the day of the week abbreviated
//System.err.println( simpleDateformat.format(today));
        //        btnDate1.setText(simpleDateformat.format(today));
        //        Date nextday = AddDays(today, 1);
        //        btnDate2.setText(simpleDateformat.format(nextday));
        //        nextday = AddDays(today, 2);
        //        btnDate3.setText(simpleDateformat.format(nextday));
        //        nextday = AddDays(today, 3);
        //        btnDate4.setText(simpleDateformat.format(nextday));
        //        nextday = AddDays(today, 4);
        //        btnDate5.setText(simpleDateformat.format(nextday));
        //        nextday = AddDays(today, 4);
        //        btnDate5.setText(simpleDateformat.format(nextday));
        //        nextday = AddDays(today, 5);
        //        btnDate6.setText(simpleDateformat.format(nextday));
    }

//    public static Date AddDays(Date date, int days) {
//        GregorianCalendar cal = new GregorianCalendar();
//        cal.setTime(date);
//        cal.add(Calendar.DATE, days);
//        return cal.getTime();
//    }
    private void SetBookingTable() {
        String[] header = new String[]{
            "Time", "Court 1", "Court 2", "Court 3", "Court 4"
        };

        //actual data for the table in a 2d array
        String[][] rows = new String[][]{
            {"07 AM", null, null, null, null},
            {"08 AM", null, null, null, null},
            {"09 AM", null, null, null, null},
            {"10 AM", null, null, null, null},
            {"11 AM", null, null, null, null},
            {"12 PM", null, null, null, null},
            {"01 PM", null, null, null, null},
            {"02 PM", null, null, null, null},
            {"03 PM", null, null, null, null},
            {"04 PM", null, null, null, null},
            {"05 PM", null, null, null, null},
            {"06 PM", null, null, null, null},
            {"07 PM", null, null, null, null}
        };

        jtblBookingPage.setCellSelectionEnabled(true);

        DefaultTableModel dtm = new DefaultTableModel(rows, header);
        jtblBookingPage.setModel(dtm);
        jtblBookingPage.setRowHeight(60);
        jtblBookingPage.getTableHeader().setPreferredSize(new Dimension(200, 40));

        jtblBookingPage.getTableHeader().setBackground(Color.CYAN);
        jtblBookingPage.getTableHeader().setForeground(Color.RED);

        jtblBookingPage.setShowVerticalLines(rootPaneCheckingEnabled);
        jtblBookingPage.setShowHorizontalLines(rootPaneCheckingEnabled);
        jtblBookingPage.setGridColor(Color.lightGray);

        SetCellsAlignment(jtblBookingPage, SwingConstants.CENTER);
        SetHeadersAlignment(jtblBookingPage, SwingConstants.CENTER);

//        //Anonymous Class like below  (Mouse Adapter is an interface)
//        MouseAdapter mouseclick = new MouseAdapter() {
//            @Override
//            public void mouseClicked(MouseEvent e) {
//                int clickedRow = jtblBookingPage.getSelectedRow();
//                int clickedColumn = jtblBookingPage.getSelectedColumn();
//            System.err.println( Integer.toString(clickedRow) + " - " + Integer.toString(clickedColumn));
//            }
//        };
//        jtblBookingPage.addMouseListener(mouseclick);
        //Listener for Mouse Click (Anonymous Class)
        jtblBookingPage.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                int clickedRow = jtblBookingPage.getSelectedRow();
                int clickedColumn = jtblBookingPage.getSelectedColumn();

                if (clickedColumn == -1) {
                    return;
                }

                JDialog.setDefaultLookAndFeelDecorated(true);
                int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to book this court?", "Confirm",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (response == JOptionPane.YES_OPTION) {
                    //buttonactive determines how many days to add from today (ie if button 6 is clicked, add 5 days
                    int activebutton = clickedbutton - 1;

//                    Date dateToday = new Date();
//                    Date bookdate = AddDays(dateToday, activebutton);
//                    LocalDate localDate = bookdate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    //Use LocalDate
                    LocalDate BookDate = LocalDate.now();
                    LocalDate localDate = BookDate.plusDays(activebutton);

                    CourtInformation ci = new CourtInformation();
                    ci.setBookYear(localDate.getYear());
                    ci.setBookMonth(localDate.getMonthValue());
                    ci.setBookDay(localDate.getDayOfMonth());
                    ci.setCourtNumber(clickedColumn);
                    ci.setCourtTime(clickedRow);
                    ci.setUserEmail(logged_email);
                    ci.setStatus(0);
                    try {
                        if (BookingsDB.CheckCourtAvailability(ci)) {    //court is available
                            JOptionPane.showMessageDialog(frame, "Court is already booked!");
                        } else if (BookingsDB.AddNewBooking(ci)) {          //Court has been booked,  create user booked coourt
//                            CourtInformation ci = new CourtInformation();
                            ci.setBookDate(BookDate);
//                            ci.setBookYear(localDate.getYear());
//                            ci.setBookMonth(localDate.getMonthValue());
//                            ci.setBookDay(localDate.getDayOfMonth());
//                            ci.setCourtNumber(clickedColumn);
//                            ci.setCourtTime(clickedRow);
//                            ci.setUserEmail(logged_email);
                            try {
                                if (!BookingsDB.addUserBooking(ci)) //Add users booking record
                                {
                                    JOptionPane.showMessageDialog(frame, "Problem creating user booked item.");
                                } else {
                                    PopulateBookingItems(BookDate);
                                }
                            } catch (DBException ex) {
                                JOptionPane.showMessageDialog(frame, ex);
                            }
                        }
                    } catch (DBException ex) {
                        JOptionPane.showMessageDialog(frame, ex);
                    }

// JOptionPane.showMessageDialog(frame, year + " " + month + " " + day);
                }
            }
        });  //end of mouse listener

    }

    public static void SetCellsAlignment(JTable table, int alignment) {
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(alignment);

        TableModel tableModel = table.getModel();

        for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++) {
            table.getColumnModel().getColumn(columnIndex).setCellRenderer(renderer);
        }
    }

    public static void SetHeadersAlignment(JTable table, int alignment) {
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(alignment);

        TableModel tableModel = table.getModel();

        for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++) {
            table.getColumnModel().getColumn(columnIndex).setHeaderRenderer(renderer);
        }
    }

    private void SetButtonColor() {
        btnDate1.setForeground(Color.black);
        btnDate2.setForeground(Color.black);
        btnDate3.setForeground(Color.black);
        btnDate4.setForeground(Color.black);
        btnDate5.setForeground(Color.black);
        btnDate6.setForeground(Color.black);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jtblBookingPage = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnDate5 = new javax.swing.JButton();
        btnDate6 = new javax.swing.JButton();
        btnDate1 = new javax.swing.JButton();
        btnDate2 = new javax.swing.JButton();
        btnDate3 = new javax.swing.JButton();
        btnDate4 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jtblBookingPage.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(jtblBookingPage);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("BOOK A COURT");

        btnDate5.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        btnDate5.setText("Sat MMM DD YYYY");
        btnDate5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDate5MouseClicked(evt);
            }
        });

        btnDate6.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        btnDate6.setText("Sat MMM DD YYYY");
        btnDate6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDate6MouseClicked(evt);
            }
        });

        btnDate1.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        btnDate1.setText("Sat MMM DD YYYY");
        btnDate1.setMargin(new java.awt.Insets(2, 8, 2, 8));
        btnDate1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDate1MouseClicked(evt);
            }
        });

        btnDate2.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        btnDate2.setText("Sat MMM DD YYYY");
        btnDate2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDate2MouseClicked(evt);
            }
        });

        btnDate3.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        btnDate3.setText("Sat MMM DD YYYY");
        btnDate3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDate3MouseClicked(evt);
            }
        });

        btnDate4.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        btnDate4.setText("Sat MMM DD YYYY");
        btnDate4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDate4MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnDate1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDate2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDate3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDate4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDate5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDate6)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnDate1)
                .addComponent(btnDate2)
                .addComponent(btnDate3)
                .addComponent(btnDate4))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnDate5)
                .addComponent(btnDate6))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane2))))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 829, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnDate1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDate1MouseClicked
        clickedbutton = 1;
        SetButtonColor();
        btnDate1.setForeground(Color.BLUE);
        LocalDate dateToday = LocalDate.now();
        PopulateBookingItems(dateToday);
    }//GEN-LAST:event_btnDate1MouseClicked

    private void btnDate2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDate2MouseClicked
        clickedbutton = 2;
        SetButtonColor();
        btnDate2.setForeground(Color.BLUE);
        LocalDate currentDate = LocalDate.now();
        LocalDate dateToday = currentDate.plusDays(1);
        PopulateBookingItems(dateToday);
    }//GEN-LAST:event_btnDate2MouseClicked

    private void btnDate3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDate3MouseClicked
        clickedbutton = 3;
        SetButtonColor();
        btnDate3.setForeground(Color.BLUE);
        LocalDate currentDate = LocalDate.now();
        LocalDate dateToday = currentDate.plusDays(2);
        PopulateBookingItems(dateToday);
    }//GEN-LAST:event_btnDate3MouseClicked

    private void btnDate4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDate4MouseClicked
        clickedbutton = 4;
        SetButtonColor();
        btnDate4.setForeground(Color.BLUE);
        LocalDate currentDate = LocalDate.now();
        LocalDate dateToday = currentDate.plusDays(3);
        PopulateBookingItems(dateToday);
    }//GEN-LAST:event_btnDate4MouseClicked

    private void btnDate5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDate5MouseClicked
        clickedbutton = 5;
        SetButtonColor();
        btnDate5.setForeground(Color.BLUE);
        LocalDate currentDate = LocalDate.now();
        LocalDate dateToday = currentDate.plusDays(4);
        PopulateBookingItems(dateToday);
    }//GEN-LAST:event_btnDate5MouseClicked

    private void btnDate6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDate6MouseClicked
        clickedbutton = 6;
        SetButtonColor();
        btnDate6.setForeground(Color.BLUE);
        LocalDate currentDate = LocalDate.now();
        LocalDate dateToday = currentDate.plusDays(5);
        PopulateBookingItems(dateToday);
    }//GEN-LAST:event_btnDate6MouseClicked

    private void PopulateBookingItems(LocalDate dateToday) {
        //Clear Table Layout cells
        String booked = "";
        for (int i = 0; i < 13; i++) {              //row
            for (int j = 1; j < 5; j++) {           //column starts at 1
                jtblBookingPage.setValueAt(booked, i, j);
            }
        }
        try {
            courtitems = BookingsDB.loadBookingInfo(dateToday);
        } catch (DBException e) {
            JOptionPane.showMessageDialog(frame, e);
        }

        if (courtitems != null) //Found booked items, display them.
        {
            for (CourtInformation ci : courtitems) {
                int time = ci.getCourtTime();         //time is used as index to gridview row

                //Convert time to timeAM/PM
                switch (time) {
                    case 0:
                        booked = "Booked " + "\n" + "07:00 AM";
                        break;
                    case 1:
                        booked = "Booked " + "\n" + "08:00 AM";
                        break;
                    case 2:
                        booked = "Booked " + "\n" + "09:00 AM";
                        break;
                    case 3:
                        booked = "Booked " + "\n" + "10:00 AM";
                        break;
                    case 4:
                        booked = "Booked " + "\n" + "11:00 AM";
                        break;
                    case 5:
                        booked = "Booked " + "\n" + "12:00 PM";
                        break;
                    case 6:
                        booked = "Booked " + "\n" + "01:00 PM";
                        break;
                    case 7:
                        booked = "Booked " + "\n" + "02:00 PM";
                        break;
                    case 8:
                        booked = "Booked " + "\n" + "03:00 PM";
                        break;
                    case 9:
                        booked = "Booked " + "\n" + "04:00 PM";
                        break;
                    case 10:
                        booked = "Booked " + "\n" + "05:00 PM";
                        break;
                    case 11:
                        booked = "Booked " + "\n" + "06:00 PM";
                        break;
                    case 12:
                        booked = "Booked " + "\n" + "07:00 PM";
                        break;
                }

                if (ci.getStatus() == 1) {
                    booked = booked + "\n Paid";
                }

                switch (ci.getCourtNumber()) {
                    case 1:
                        jtblBookingPage.setValueAt(booked, time, 1);
                        break;
                    case 2:
                        jtblBookingPage.setValueAt(booked, time, 2);
                        break;
                    case 3:
                        jtblBookingPage.setValueAt(booked, time, 3);
                        break;
                    case 4:
                        jtblBookingPage.setValueAt(booked, time, 4);
                        break;
                }
            }
        }

    }

//    /**
//     * @param <code>args[]</code>  the command line arguments
//     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(frmBookingPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(frmBookingPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(frmBookingPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(frmBookingPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new frmBookingPage().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDate1;
    private javax.swing.JButton btnDate2;
    private javax.swing.JButton btnDate3;
    private javax.swing.JButton btnDate4;
    private javax.swing.JButton btnDate5;
    private javax.swing.JButton btnDate6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jtblBookingPage;
    // End of variables declaration//GEN-END:variables
}
