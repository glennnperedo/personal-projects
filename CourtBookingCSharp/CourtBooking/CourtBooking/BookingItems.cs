﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtBooking
{
    public partial class CourtInformation   //How about using Struc instead of Class?
    {
        public DateTime BookDate { get; set; }
        public int BookYear { get; set; }
        public int BookMonth { get; set; }
        public int BookDay { get; set; }
        public int CourtNumber { get; set; }
        public int CourtTime { get; set; }
        public string UserEmail { get; set; }
        public int Status { get; set; }
    }

    //public partial class CourtAvailability
    //{
    //    public int BookYear { get; set; }
    //    public int BookMonth { get; set; }
    //    public int BookDay { get; set; }
    //    public int CourtNumber { get; set; }
    //    public int CourtTime { get; set; }
    //    public string UserEmail { get; set; }      
    //}

    //public partial class UserBookedCourts
    //{
    //    public string UserEmail { get; set; }
    //    public DateTime BookDate { get; set; }
    //    //year,month,day could be derived from BookDate above. Will leave for now.
    //    public int BookYear { get; set; }       
    //    public int BookMonth { get; set; }
    //    public int BookDay { get; set; }
    //    public int CourtNumber { get; set; }
    //    public int CourtTime { get; set; }
    //}

}
