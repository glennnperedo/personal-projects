﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourtBooking
{
    public partial class frmViewCancelBookedCourt : Form
    {
        public   List<CourtInformation> savedCourtInformation = new List<CourtInformation>();       //Inforamtion here was passed from main form
        public   string user_email;                                                                 //pass to cancel routine

        public frmViewCancelBookedCourt()
        {
            InitializeComponent();
        }

        private void frmViewCancelBookedCourt_Load(object sender, EventArgs e)
        {
            //this.ViewBookedCourt();
            this.dgvUserBookedCourts.CellClick += new DataGridViewCellEventHandler(this.dgvUserBookedCourts_CellClick);     //create event handler
            SetupDataGridView();
        }

        public void SetupDataGridView()
        {
            int listcount = savedCourtInformation.Count;                         //number of records selected  from database
            dgvUserBookedCourts.RowCount = listcount;

            for (int i = 0; i < listcount; i++)
            {
                DataGridViewButtonCell payCell = new DataGridViewButtonCell();
                payCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                payCell.FlatStyle = FlatStyle.Popup;
                payCell.Style.BackColor = Color.LightSkyBlue;
                DataGridViewButtonCell cancelCell = new DataGridViewButtonCell();
                cancelCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                cancelCell.FlatStyle = FlatStyle.Popup;
                cancelCell.Style.BackColor = Color.LightGray;

                DateTime dateBooked = Convert.ToDateTime(savedCourtInformation[i].BookDate);
                this.dgvUserBookedCourts.Rows[i].Cells[0].Value = dateBooked.ToString("ddd") + ", " + dateBooked.ToString("MMM dd, yyyy");
                this.dgvUserBookedCourts.Rows[i].Cells[1].Value = ConvertTimeToString(savedCourtInformation[i].CourtTime.ToString());
                this.dgvUserBookedCourts.Rows[i].Cells[2].Value = savedCourtInformation[i].CourtNumber.ToString();
                this.dgvUserBookedCourts.Rows[i].Cells[3] = payCell;
                this.dgvUserBookedCourts.Rows[i].Cells[4] = cancelCell;
                this.dgvUserBookedCourts.Rows[i].Cells[4].Value = "Cancel";
                this.dgvUserBookedCourts.Columns[4].Name = "Cancel";

                if (savedCourtInformation[i].Status == 1)
                {
                    this.dgvUserBookedCourts.Rows[i].Cells[3].Value = "Paid";
                    this.dgvUserBookedCourts.Rows[i].Cells[3].Style.BackColor = Color.LightYellow;
                }
                else
                {
                    this.dgvUserBookedCourts.Rows[i].Cells[3].Value = "Pay";
                    this.dgvUserBookedCourts.Columns[3].Name = "Pay";
                }
            }

            //-------------------------------------------
            //Set grid height dependng on number of rows
            //-------------------------------------------
            int rowheight = dgvUserBookedCourts.Rows[0].Height;
            dgvUserBookedCourts.Height = (rowheight * listcount) + dgvUserBookedCourts.ColumnHeadersHeight;
            dgvUserBookedCourts.ClearSelection();     //Clear focus

        }

        private void dgvUserBookedCourts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            CourtInformation courttopay = new CourtInformation();
            bool updateOk = false;
            const int PAY = 1;
            const int CANCEL = 2;

            //Pay or Cancel only allowed,  else do nothing
            if (e.ColumnIndex == 3 && e.RowIndex > -1)     //Pay the court
            {
                if(dgvUserBookedCourts.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "Pay")
                {
                    courttopay = savedCourtInformation[e.RowIndex];
                    courttopay.Status = PAY;

                    updateOk = BookingsDB.UpdateBookedCourt(courttopay, user_email);
                    if (updateOk)
                    {
                        dgvUserBookedCourts.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "Paid";
                        this.dgvUserBookedCourts.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.LightYellow;
                        MessageBox.Show("Court Paid.", "Pay booked court");
                    }
                }
            }
            else if (e.ColumnIndex == 4 && e.RowIndex > -1)    //Cancel the court
            {
                CourtInformation courttocancel = new CourtInformation();
                courttocancel = savedCourtInformation[e.RowIndex];
                courttocancel.Status = CANCEL;

                if (BookingsDB.UpdateBookedCourt(courttocancel, user_email))
                {
                    MessageBox.Show("Court canceled.", "Cancel booked court");

                    //Reload information
                    savedCourtInformation = BookingsDB.GetUserBooking(user_email);        //Fetch Booked Courts for this user

                    if (savedCourtInformation != null)
                    {
                        SetupDataGridView();
                    }
                    else
                    {
                        MessageBox.Show("You have no booked court.", "Display booked court");
                        this.Dispose();
                    }
                }
                else
                    MessageBox.Show("Court cancel failed.", "Cancel booked court");
            }

            dgvUserBookedCourts.ClearSelection();     //disable focus
        }

        public string ConvertTimeToString(string time)
        {
            string stringtime = "";

            switch (time)
            {
                case "0": stringtime = "07 AM"; break;
                case "1": stringtime = "08 AM"; break;
                case "2": stringtime = "09 AM"; break;
                case "3": stringtime = "10 AM"; break;
                case "4": stringtime = "11 AM"; break;
                case "5": stringtime = "12 PM"; break;
                case "6": stringtime = "01 PM"; break;
                case "7": stringtime = "02 PM"; break;
                case "8": stringtime = "03 PM"; break;
                case "9": stringtime = "04 PM"; break;
                case "10": stringtime = "05 PM"; break;
                case "11": stringtime = "06 PM"; break;
                case "12": stringtime = "07 PM"; break;
            }
            return stringtime;
        }

        //-----------------------------------------------------------------------------------------
        //TableLayoutPanel is used below.  It requires a lot more coding than the above gridview table
        //to achieve the same result.
        //-----------------------------------------------------------------------------------------
        public void ViewBookedCourt()      //load booked courts max of six lines.
        {
            //InitializeComponent();
            btnCancel2.Visible = false;
            btnCancel3.Visible = false;
            btnCancel4.Visible = false;
            btnCancel5.Visible = false;
            btnCancel6.Visible = false;
            btnPay2.Visible = false;
            btnPay3.Visible = false;
            btnPay4.Visible = false;
            btnPay5.Visible = false;
            btnPay6.Visible = false;

            int listcount = savedCourtInformation.Count;    //Total court booked

            for (int i = 0; i < listcount; i++)
            {
                if (i == 0)
                {
                    txtCourt1.Text = savedCourtInformation[i].CourtNumber.ToString();
                    DateTime dateBooked = Convert.ToDateTime(savedCourtInformation[i].BookDate);
                    txtDate1.Text = dateBooked.ToString("ddd") + ", " + dateBooked.ToString("MMM dd, yyyy");
                    txtTime1.Text = ConvertTimeToString(savedCourtInformation[i].CourtTime.ToString());

                    if (savedCourtInformation[i].Status == 1)
                    {
                        btnPay1.Enabled = false;        //paid already. 
                        btnPay1.Text = "Paid";
                    }

                }
                else if (i == 1)
                {
                    txtCourt2.Text = savedCourtInformation[i].CourtNumber.ToString(); ;
                    DateTime dateBooked = Convert.ToDateTime(savedCourtInformation[i].BookDate);
                    txtDate2.Text = dateBooked.ToString("ddd") + ", " + dateBooked.ToString("MMM dd, yyyy");
                    txtTime2.Text = ConvertTimeToString(savedCourtInformation[i].CourtTime.ToString());
                    btnCancel2.Visible = true;
                    btnPay2.Visible = true;

                    if (savedCourtInformation[i].Status == 1)
                    {
                        btnPay2.Enabled = false;        //paid already. 
                        btnPay2.Text = "Paid";
                    }
                }
                else if (i == 2)
                {
                    txtCourt3.Text = savedCourtInformation[i].CourtNumber.ToString(); ;
                    DateTime dateBooked = Convert.ToDateTime(savedCourtInformation[i].BookDate);
                    txtDate3.Text = dateBooked.ToString("ddd") + ", " + dateBooked.ToString("MMM dd, yyyy");
                    txtTime3.Text = ConvertTimeToString(savedCourtInformation[i].CourtTime.ToString());
                    btnCancel3.Visible = true;
                    btnPay3.Visible = true;
                    if (savedCourtInformation[i].Status == 1)
                    {
                        btnPay3.Enabled = false;        //paid already. 
                        btnPay3.Text = "Paid";
                    }
                }
                else if (i == 3)
                {
                    txtCourt4.Text = savedCourtInformation[i].CourtNumber.ToString(); ;
                    DateTime dateBooked = Convert.ToDateTime(savedCourtInformation[i].BookDate);
                    txtDate4.Text = dateBooked.ToString("ddd") + ", " + dateBooked.ToString("MMM dd, yyyy");
                    txtTime4.Text = ConvertTimeToString(savedCourtInformation[i].CourtTime.ToString());
                    btnCancel4.Visible = true;
                    btnPay4.Visible = true;
                    if (savedCourtInformation[i].Status == 1)
                    {
                        btnPay4.Enabled = false;        //paid already. 
                        btnPay4.Text = "Paid";
                    }
                }
                else if (i == 4)
                {
                    txtCourt5.Text = savedCourtInformation[i].CourtNumber.ToString(); ;
                    DateTime dateBooked = Convert.ToDateTime(savedCourtInformation[i].BookDate);
                    txtDate5.Text = dateBooked.ToString("ddd") + ", " + dateBooked.ToString("MMM dd, yyyy");
                    txtTime5.Text = ConvertTimeToString(savedCourtInformation[i].CourtTime.ToString());
                    btnCancel5.Visible = true;
                    btnPay5.Visible = true;
                    if (savedCourtInformation[i].Status == 1)
                    {
                        btnPay5.Enabled = false;        //paid already. 
                        btnPay5.Text = "Paid";
                    }
                }
                else if (i == 5)
                {
                    txtCourt6.Text = savedCourtInformation[i].CourtNumber.ToString(); ;
                    DateTime dateBooked = Convert.ToDateTime(savedCourtInformation[i].BookDate);
                    txtDate6.Text = dateBooked.ToString("ddd") + ", " + dateBooked.ToString("MMM dd, yyyy");
                    txtTime6.Text = ConvertTimeToString(savedCourtInformation[i].CourtTime.ToString());
                    btnCancel6.Visible = true;
                    btnPay6.Visible = true;
                    if (savedCourtInformation[i].Status == 1)
                    {
                        btnPay6.Enabled = false;        //paid already. 
                        btnPay6.Text = "Paid";
                    }
                }

            }
        }

        private void PayButton_Click(object sender, EventArgs e)
        {
            CourtInformation courttopay = new CourtInformation();
            bool updateOk = false;
            const int PAID = 1;

            switch (((Button)sender).Name)
            {
                case "btnPay1":
                    courttopay = savedCourtInformation[0];
                    courttopay.Status = PAID;
                    updateOk = BookingsDB.UpdateBookedCourt(courttopay, user_email);
                    if (updateOk)
                    {
                        btnPay1.Text = "Paid";
                        btnPay1.Enabled = false;
                        MessageBox.Show("Court Paid.", "Pay booked court");
                    }
                    break;
                case "btnPay2":
                    courttopay = savedCourtInformation[1];
                    courttopay.Status = PAID;
                    updateOk = BookingsDB.UpdateBookedCourt(courttopay, user_email);
                    if (updateOk)
                    {
                        btnPay2.Text = "Paid";
                        btnPay2.Enabled = false;
                        MessageBox.Show("Court Paid.", "Pay booked court");
                    }
                    break;

                case "btnPay3":
                    courttopay = savedCourtInformation[2];
                    courttopay.Status = PAID;
                    updateOk = BookingsDB.UpdateBookedCourt(courttopay, user_email);
                    if (updateOk)
                    {
                        btnPay3.Text = "Paid";
                        btnPay3.Enabled = false;
                        MessageBox.Show("Court Paid.", "Pay booked court");
                    }
                    break;
                case "btnPay4":
                    courttopay = savedCourtInformation[3];
                    courttopay.Status = PAID;
                    updateOk = BookingsDB.UpdateBookedCourt(courttopay, user_email);
                    if (updateOk)
                    {
                        btnPay4.Text = "Paid";
                        btnPay4.Enabled = false;
                        MessageBox.Show("Court Paid.", "Pay booked court");
                    }
                    break;
                case "btnPay5":
                    courttopay = savedCourtInformation[4];
                    courttopay.Status = PAID;
                    updateOk = BookingsDB.UpdateBookedCourt(courttopay, user_email);
                    if (updateOk)
                    {
                        btnPay5.Text = "Paid";
                        btnPay5.Enabled = false;
                        MessageBox.Show("Court Paid.", "Pay booked court");
                    }
                    break;
                case "btnPay6":
                    courttopay = savedCourtInformation[5];
                    courttopay.Status = PAID;
                    updateOk = BookingsDB.UpdateBookedCourt(courttopay, user_email);
                    if (updateOk)
                    {
                        btnPay6.Text = "Paid";
                        btnPay6.Enabled = false;
                        MessageBox.Show("Court Paid.", "Pay booked court");
                    }
                    break;
            }

        }

        private void btnCancel1_Click(object sender, EventArgs e)
        {
            //Cancel first line (index 0)
            //BookingsDB booking = new BookingsDB();

            CourtInformation courttocancel = new CourtInformation();
            courttocancel = savedCourtInformation[0];
            courttocancel.Status = 2;

            if (BookingsDB.UpdateBookedCourt(courttocancel, user_email))
            {
                MessageBox.Show("Court canceled.", "Cancel booked court");
                txtDate1.Text = "";
                txtTime1.Text = "";
                txtCourt1.Text = "";
                btnCancel1.Visible = false;
                btnPay1.Visible = false;
            }
            else
                MessageBox.Show("Court cancel failed.", "Cancel booked court");

        }
        private void btnCancel2_Click(object sender, EventArgs e)
        {
            //Cancel second line (index 1)
            //BookingsDB booking = new BookingsDB();
            CourtInformation courttocancel = new CourtInformation();
            courttocancel = savedCourtInformation[1];
            courttocancel.Status = 2;

            if (BookingsDB.UpdateBookedCourt(courttocancel, user_email))
            {
                MessageBox.Show("Court canceled.", "Cancel booked court");
                txtDate2.Text = "";
                txtTime2.Text = "";
                txtCourt2.Text = "";
                btnCancel2.Visible = false;
                btnPay2.Visible = false;
            }
            else
                MessageBox.Show("Court cancel failed.", "Cancel booked court");
        }
        private void btnCancel3_Click(object sender, EventArgs e)
        {
            //Cancel third line (index 2)
            //BookingsDB booking = new BookingsDB();
            CourtInformation courttocancel = new CourtInformation();
            courttocancel = savedCourtInformation[2];
            courttocancel.Status = 2;

            if (BookingsDB.UpdateBookedCourt(courttocancel, user_email))
            {
                MessageBox.Show("Court canceled.", "Cancel booked court");
                txtDate3.Text = "";
                txtTime3.Text = "";
                txtCourt3.Text = "";
                btnCancel3.Visible = false;
                btnPay3.Visible = false;
            }
            else
                MessageBox.Show("Court cancel failed.", "Cancel booked court");
        }
        private void btnCancel4_Click(object sender, EventArgs e)
        {
            //Cancel 4th line (index 3)
            //BookingsDB booking = new BookingsDB();
            CourtInformation courttocancel = new CourtInformation();
            courttocancel = savedCourtInformation[3];
            courttocancel.Status = 2;

            if (BookingsDB.UpdateBookedCourt(courttocancel, user_email))
            {
                MessageBox.Show("Court canceled.", "Cancel booked court");
                txtDate4.Text = "";
                txtTime4.Text = "";
                txtCourt4.Text = "";
                btnCancel4.Visible = false;
                btnPay4.Visible = false;
            }
            else
                MessageBox.Show("Court cancel failed.", "Cancel booked court");
        }
        private void btnCancel5_Click(object sender, EventArgs e)
        {
            //Cancel 5th line (index 4)
            //BookingsDB booking = new BookingsDB();
            CourtInformation courttocancel = new CourtInformation();
            courttocancel = savedCourtInformation[4];
            courttocancel.Status = 2;

            if (BookingsDB.UpdateBookedCourt(courttocancel, user_email))
            {
                MessageBox.Show("Court canceled.", "Cancel booked court");
                txtDate5.Text = "";
                txtTime5.Text = "";
                txtCourt5.Text = "";
                btnCancel5.Visible = false;
                btnPay5.Visible = false;
            }
            else
                MessageBox.Show("Court cancel failed.", "Cancel booked court");
        }
        private void btnCancel6_Click(object sender, EventArgs e)
        {
            //Cancel 6th line (index 5)
            //BookingsDB booking = new BookingsDB();
            CourtInformation courttocancel = new CourtInformation();
            courttocancel = savedCourtInformation[5];
            courttocancel.Status = 2;

            if (BookingsDB.UpdateBookedCourt(courttocancel, user_email))
            {
                MessageBox.Show("Court canceled.", "Cancel booked court");
                txtDate6.Text = "";
                txtTime6.Text = "";
                txtCourt6.Text = "";
                btnCancel6.Visible = false;
                btnPay6.Visible = false;
            }
            else
                MessageBox.Show("Court cancel failed.", "Cancel booked court");
        }

    }
}

