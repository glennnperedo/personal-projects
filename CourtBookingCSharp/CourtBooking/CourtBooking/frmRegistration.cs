﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourtBooking
{
    public partial class frmRegistration : Form
    {
        public frmRegistration()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //If present validation.  Must add extra validation on phone, password
            if (string.IsNullOrEmpty(txtFirstName.Text) 
                || string.IsNullOrEmpty(txtLastName.Text) 
                || string.IsNullOrEmpty(txtEmail.Text)
                || string.IsNullOrEmpty(txtPassword.Text)
                || string.IsNullOrEmpty(txtAddress.Text)
                || string.IsNullOrEmpty(txtPhone.Text))
                MessageBox.Show("All fields are required.");
            else
            {
                UserDB.FirstName = txtFirstName.Text;
                UserDB.LastName = txtLastName.Text;
                UserDB.Email = txtEmail.Text;
                UserDB.Password = txtPassword.Text;
                UserDB.Address = txtAddress.Text;
                UserDB.Phone = txtPhone.Text;

                if (UserDB.Register())
                {
                    MessageBox.Show("Registration completed successfully.");
                    this.DialogResult = DialogResult.OK;
                    //this.Close();
                    this.Dispose(true);
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            //this.Close();
            this.Dispose(true);
        }

        private void frmRegistration_Load(object sender, EventArgs e)
        {

        }

    }
}
