﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace CourtBooking
{
    public partial class frmBookingPage : Form
    {
        public List<CourtInformation> courtitems = new List<CourtInformation>();    
        public string logged_email { get; set; }                //Logged user email passed from frmMain
        public string logged_firstname { get; set; }
        public string logged_lastname { get; set; }

        int clickedButton;                                      //Which Date button was clicked

        public frmBookingPage()
        {
            InitializeComponent();
            PrepDataGridvView();
            PrepBookDateButtons();
        }

        private void frmBookingPage_Load(object sender, EventArgs e)
        {
            clickedButton = 1;
            btnDate1.ForeColor = Color.Blue;
            PopulateBookingItems(DateTime.Today);   // load booking information starting from today's date
            dgvCourtBooking.ClearSelection();

            lblName.Text = logged_firstname + " " + logged_lastname;    // display user
        }

        // load dates on the 6 date buttons
        // 1 day is added from today's date and so on
        // and display the date on each button
        private void PrepBookDateButtons()
        {
            DateTime dateToday = DateTime.Today;
            string DateDisplay = dateToday.ToString("ddd") + ", " + dateToday.ToString("MMM dd, yyyy");
            btnDate1.Text = DateDisplay;

            dateToday = dateToday.AddDays(1);   
            DateDisplay = dateToday.ToString("ddd") + ", " + dateToday.ToString("MMM dd, yyyy");
            btnDate2.Text = DateDisplay;

            dateToday = dateToday.AddDays(1);
            DateDisplay = dateToday.ToString("ddd") + ", " + dateToday.ToString("MMM dd, yyyy");
            btnDate3.Text = DateDisplay;

            dateToday = dateToday.AddDays(1);
            DateDisplay = dateToday.ToString("ddd") + ", " + dateToday.ToString("MMM dd, yyyy");
            btnDate4.Text = DateDisplay;

            dateToday = dateToday.AddDays(1);
            DateDisplay = dateToday.ToString("ddd") + ", " + dateToday.ToString("MMM dd, yyyy");
            btnDate5.Text = DateDisplay;

            dateToday = dateToday.AddDays(1);
            DateDisplay = dateToday.ToString("ddd") + ", " + dateToday.ToString("MMM dd, yyyy");
            btnDate6.Text = DateDisplay;

            //MessageBox.Show(dateToday.ToString("MMM dd, yyyy"));
            //MessageBox.Show(dateToday.ToString("D", CultureInfo.CreateSpecificCulture("en-US")));
        }


        //  This is main booking event
        private void dgvCourtBooking_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 0 || e.RowIndex == -1)      // ignore Time header column click
            {
                return;
            }
            //MessageBox.Show(string.Format("row: {0}, column: {1}", e.RowIndex, e.ColumnIndex));
            //if (dgvCourtBooking.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() != "")
            //{
            //    MessageBox.Show("The court is already booked!");
            //    return;
            //}

            DialogResult result = MessageBox.Show("Are you sure you want to book this court?", "Book a Court", 
                                                   MessageBoxButtons.YesNo,  
                                                   MessageBoxIcon.Question,  
                                                   MessageBoxDefaultButton.Button2);
            if(result == DialogResult.Yes)
            {
                if (e.ColumnIndex > 0 && e.RowIndex >= 0)       // column 0 is time row header
                {
                    BookTheCourt(e.ColumnIndex, e.RowIndex);    // pass the matrix
                }
                dgvCourtBooking.ClearSelection();
            }
       

        }

        // add user booking entry after user clicked on the booking table
        private void BookTheCourt(int colidx, int rowidx)
        {
            DateTime dateToday = DateTime.Today;

            //buttonactive determines how many days to add from today (ie if button 6 is clicked, add 5 days)
            int buttonactive = clickedButton - 1;   // set active button less 1 which is used to add to today's date
            dateToday = dateToday.AddDays(buttonactive);

            CourtInformation ca = new CourtInformation();
            ca.BookYear = dateToday.Year;
            ca.BookMonth = dateToday.Month;
            ca.BookDay = dateToday.Day;
            ca.CourtNumber = colidx; ;
            ca.CourtTime = rowidx;
            ca.UserEmail = logged_email;
            ca.Status = 0;

            if (BookingsDB.CheckCourtAvailability(ca))
            {
                if (BookingsDB.AddNewBooking(ca))
                {
                    CourtInformation ubc = new CourtInformation();
                    ubc.BookDate = dateToday;
                    ubc.BookYear = dateToday.Year;
                    ubc.BookMonth = dateToday.Month;
                    ubc.BookDay = dateToday.Day;
                    ubc.CourtNumber = colidx; ;
                    ubc.CourtTime = rowidx;
                    ubc.UserEmail = logged_email;

                    if (!BookingsDB.AddUserBooking(ubc))      // add users booking record
                    {
                        MessageBox.Show("Problem creating user booked item.", "Add User Booked Court");
                    }
                    PopulateBookingItems(dateToday);
                }
                else
                    MessageBox.Show("Problem encountered booking the court", "Book a court");
            }
        }

        // user clicked the date button 
        // the table is then populated with the 
        // booked courts for the selected date
        private void btnDate1_Click(object sender, EventArgs e)
        {
            clickedButton = 1;  // assign the clicked button
            ClearButtonColor();
            btnDate1.ForeColor = Color.Blue;
            DateTime dateToday = DateTime.Today;
            PopulateBookingItems(dateToday);
        }

        private void btnDate2_Click(object sender, EventArgs e)
        {
            clickedButton = 2;
            ClearButtonColor();
            btnDate2.ForeColor = Color.Blue;
            DateTime dateToday = DateTime.Today;
            dateToday = dateToday.AddDays(1);
            PopulateBookingItems(dateToday);
        }

        private void btnDate3_Click(object sender, EventArgs e)
        {
            clickedButton = 3;
            ClearButtonColor();
            btnDate3.ForeColor = Color.Blue;
            DateTime dateToday = DateTime.Today;
            dateToday = dateToday.AddDays(2);
            PopulateBookingItems(dateToday);
        }

        private void btnDate4_Click(object sender, EventArgs e)
        {
            clickedButton = 4;
            ClearButtonColor();
            btnDate4.ForeColor = Color.Blue;
            DateTime dateToday = DateTime.Today;
            dateToday = dateToday.AddDays(3);
            PopulateBookingItems(dateToday);
        }

        private void btnDate5_Click(object sender, EventArgs e)
        {
            clickedButton = 5;
            ClearButtonColor();
            btnDate5.ForeColor = Color.Blue;
            DateTime dateToday = DateTime.Today;
            dateToday = dateToday.AddDays(4);
            PopulateBookingItems(dateToday);
        }

        private void btnDate6_Click(object sender, EventArgs e)
        {
            clickedButton = 6;
            ClearButtonColor();
            btnDate6.ForeColor = Color.Blue;
            DateTime dateToday = DateTime.Today;
            dateToday = dateToday.AddDays(5);
            PopulateBookingItems(dateToday);
        }

        private void PopulateBookingItems(DateTime dateToday)
        {
            //Clear gridview cells
            string booked = "";
            for (int i = 0; i < 13; i ++)
            {
                for (int j = 1; j < 5; j++)
                {
                    dgvCourtBooking.Rows[i].Cells[j].Value = booked;
                }
            }

            //courtitems object (class variable) contains booking information for 4 courts for the passed Date dateToday
            courtitems = BookingsDB.LoadBookingInfo(dateToday);   

            if (courtitems != null)       // found booked items, display them.
            {
                foreach (CourtInformation items in courtitems)
                {
                    int time = Convert.ToByte(items.CourtTime);         //time is used as index to gridview row
                    
                    //Convert time to timeAM/PM
                    switch(time)
                    {
                        case 0: booked = "Booked" + "\n" + "07:00 AM"; break;
                        case 1: booked = "Booked" + "\n" + "08:00 AM"; break;
                        case 2: booked = "Booked" + "\n" + "09:00 AM"; break;
                        case 3: booked = "Booked" + "\n" + "10:00 AM"; break;
                        case 4: booked = "Booked" + "\n" + "11:00 AM"; break;
                        case 5: booked = "Booked" + "\n" + "12:00 PM"; break;
                        case 6: booked = "Booked" + "\n" + "01:00 PM"; break;
                        case 7: booked = "Booked" + "\n" + "02:00 PM"; break;
                        case 8: booked = "Booked" + "\n" + "03:00 PM"; break;
                        case 9: booked = "Booked" + "\n" + "04:00 PM"; break;
                        case 10: booked = "Booked" + "\n" + "05:00 PM"; break;
                        case 11: booked = "Booked" + "\n" + "06:00 PM"; break;
                        case 12: booked = "Booked" + "\n" + "07:00 PM"; break;
                    }

                    if(items.Status == 1)   // status 1 is paid
                        booked = booked + "\n Paid"; 

                    // display court as booked
                    switch (items.CourtNumber)
                    {
                        case 1:
                            dgvCourtBooking.Rows[time].Cells[1].Value = booked;
                            break;
                        case 2:
                            dgvCourtBooking.Rows[time].Cells[2].Value = booked;
                            break;
                        case 3:
                            dgvCourtBooking.Rows[time].Cells[3].Value = booked;
                            break;
                        case 4:
                            dgvCourtBooking.Rows[time].Cells[4].Value = booked;
                            break;
                    }
                }
            }
           
        }

        private void ClearButtonColor()
        {
            btnDate1.ForeColor = Color.Black;
            btnDate2.ForeColor = Color.Black;
            btnDate3.ForeColor = Color.Black;
            btnDate4.ForeColor = Color.Black;
            btnDate5.ForeColor = Color.Black;
            btnDate6.ForeColor = Color.Black;
        }

        private void dgvCourtBooking_CellDoubleClick(Object sender, DataGridViewCellEventArgs e)
        {
            //MessageBox.Show("dbl click");
            dgvCourtBooking.ClearSelection();
        }

        private void PrepDataGridvView()
        {
            dgvCourtBooking.ColumnCount = 5;
            dgvCourtBooking.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dgvCourtBooking.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dgvCourtBooking.ColumnHeadersHeight = 30;
            dgvCourtBooking.Columns[0].Name = "Time";
            dgvCourtBooking.Columns[1].Name = "Court 1";
            dgvCourtBooking.Columns[2].Name = "Court 2";
            dgvCourtBooking.Columns[3].Name = "Court 3";
            dgvCourtBooking.Columns[4].Name = "Court 4";

            dgvCourtBooking.Columns[0].Width = 100;
            dgvCourtBooking.Columns[1].Width = 180;
            dgvCourtBooking.Columns[2].Width = 180;
            dgvCourtBooking.Columns[3].Width = 180;
            dgvCourtBooking.Columns[4].Width = 180;

            dgvCourtBooking.Columns[0].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCourtBooking.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCourtBooking.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCourtBooking.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCourtBooking.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgvCourtBooking.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCourtBooking.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCourtBooking.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCourtBooking.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCourtBooking.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvCourtBooking.ColumnHeadersDefaultCellStyle.Font = new Font(DataGridView.DefaultFont, FontStyle.Bold);


            foreach (DataGridViewColumn column in dgvCourtBooking.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
                column.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            }

            dgvCourtBooking.RowHeadersVisible = false;

            dgvCourtBooking.RowTemplate.Height = 46;
            dgvCourtBooking.RowCount = 13;

            dgvCourtBooking.Rows[0].Cells[0].Value = "07:00 AM";
            dgvCourtBooking.Rows[1].Cells[0].Value = "08:00 AM";
            dgvCourtBooking.Rows[2].Cells[0].Value = "09:00 AM";
            dgvCourtBooking.Rows[3].Cells[0].Value = "10:00 AM";
            dgvCourtBooking.Rows[4].Cells[0].Value = "11:00 AM";
            dgvCourtBooking.Rows[5].Cells[0].Value = "12:00 PM";
            dgvCourtBooking.Rows[6].Cells[0].Value = "01:00 PM";
            dgvCourtBooking.Rows[7].Cells[0].Value = "02:00 PM";
            dgvCourtBooking.Rows[8].Cells[0].Value = "03:00 PM";
            dgvCourtBooking.Rows[9].Cells[0].Value = "04:00 PM";
            dgvCourtBooking.Rows[10].Cells[0].Value = "05:00 PM";
            dgvCourtBooking.Rows[11].Cells[0].Value = "06:00 PM";
            dgvCourtBooking.Rows[12].Cells[0].Value = "07:00 PM";
            dgvCourtBooking.Columns["Time"].DefaultCellStyle.ForeColor = Color.Red;

            dgvCourtBooking.CellDoubleClick += new DataGridViewCellEventHandler(dgvCourtBooking_CellDoubleClick);
            dgvCourtBooking.CellClick += new DataGridViewCellEventHandler(dgvCourtBooking_CellClick);    //enable cell_click handler
        }

        // re-populate the table after update
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            DateTime dateToday = DateTime.Today;
            int buttonactive = clickedButton - 1;
            dateToday = dateToday.AddDays(buttonactive);
            PopulateBookingItems(dateToday);
        }
    }
}
 