﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CourtBooking
{
    public static class UserDB
    {
        public  static string FirstName { get; set; }
        public  static string LastName { get; set; }
        public  static string Address { get; set; }
        public  static string Phone { get; set; }
        public  static string Email { get; set; }
        public  static string Password { get; set; }

        public  static bool Login()
        {

            SqlConnection connection = ConnectDB.GetConnection();

            string selectStatement = "SELECT * FROM tblRegisteredUsers " +
                                     "WHERE UserEmail = @UserEmail AND UserPassword = @UserPassword";

            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);

            selectCommand.Parameters.AddWithValue("@UserEmail", Email);
            selectCommand.Parameters.AddWithValue("@UserPassword", Password);

            try
            {
                connection.Open();

                SqlDataReader reader = selectCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        FirstName = reader["UserFirstName"].ToString();
                        LastName = reader["UserLastName"].ToString();
                    }
                    reader.Close();
                    return true;                  
                }
                else
                    return false;
            }

            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                return false;
            }

            finally
            {
                connection.Close();
            }
        }

        public static bool Register()
        {
            //string hashedpassword = FormsAuthentication.HashPasswordForStoringInConfigFile(Password, "SHA1");
            SqlConnection connection = ConnectDB.GetConnection();
            string insertStatement = "INSERT INTO tblRegisteredUsers " +
                                      "(UserEmail, UserFirstName, UserLastName, UserPassword, UserAddress, UserPhone)" +
                                      "VALUES (@UserEmail, @UserFirstName,@UserLastName, @UserPassword, @UserAddress, @UserPhone)";

            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);

            insertCommand.Parameters.AddWithValue("@UserEmail", Email);
            insertCommand.Parameters.AddWithValue("@UserFirstName", FirstName);
            insertCommand.Parameters.AddWithValue("@UserLastName", LastName);
            insertCommand.Parameters.AddWithValue("@UserPassword", Password);
            insertCommand.Parameters.AddWithValue("@UserAddress", Address);
            insertCommand.Parameters.AddWithValue("@UserPhone", Phone);

            try
            {
                connection.Open();
                insertCommand.ExecuteNonQuery();
                return true;

            }
            //catch (OleDbException ex)
            catch (SqlException ex)
            {
                if (ex.Number == 2627) // duplicate key
                    MessageBox.Show("The Email Address is already registered.");
                else
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                return false;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
