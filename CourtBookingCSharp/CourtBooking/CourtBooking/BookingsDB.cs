﻿//  LoadBookingInfo - Displays bookings on a psrticular date
//   AddNewBooking - creates a booked court for a given date

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourtBooking
{
    public static class BookingsDB
    {
        public static List<CourtInformation> LoadBookingInfo(DateTime bookdate)
        {
            int bookyear = bookdate.Year;
            int bookmonth = bookdate.Month;
            int bookday = bookdate.Day;
            List<CourtInformation> courtitems = new List<CourtInformation>();

            SqlConnection connection = ConnectDB.GetConnection();
            string selectstatement = "Select *  FROM tblBookings " +
                                    "WHERE BookYear = @BookYear AND BookMonth = @BookMonth AND BookDay = @BookDay";

            SqlCommand selectCommand = new SqlCommand(selectstatement, connection);

            selectCommand.Parameters.AddWithValue("@BookYear", bookyear);
            selectCommand.Parameters.AddWithValue("@BookMonth", bookmonth);
            selectCommand.Parameters.AddWithValue("@BookDay", bookday);

            try
            {
                connection.Open();

                SqlDataReader reader = selectCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CourtInformation ca = new CourtInformation();
                        ca.BookYear = (int)reader["BookYear"];
                        ca.BookYear = (int)reader["BookMonth"];
                        ca.BookYear = (int)reader["BookDay"];
                        ca.CourtNumber = (int)reader["CourtNumber"];
                        ca.CourtTime = (int)reader["CourtTime"];
                        ca.UserEmail = reader["UserEmail"].ToString();
                        ca.Status = (int)reader["Status"];
                        courtitems.Add(ca);
                    }
                    reader.Close();
                }
                else
                {
                    return null;
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            finally
            {
                connection.Close();
            }
            return courtitems;
        }
       
        public static bool AddUserBooking(CourtInformation ci)
        {
            SqlConnection connection = ConnectDB.GetConnection();
            
            string insertstatement = "INSERT INTO tblUserBookedCourts " +
                                     "(UserEmail, BookYear, BookMonth, BookDay, BookCourt, BookTime, BookDate, BookedDate, Status ) " +
                                     "VALUES (@UserEmail, @BookYear, @BookMonth, @BookDay, @BookCourt, @BookTime, @BookDate, @BookedDate, @Status)";

            SqlCommand insertCommand = new SqlCommand(insertstatement, connection);

            insertCommand.Parameters.AddWithValue("@UserEmail", ci.UserEmail);
            insertCommand.Parameters.AddWithValue("@BookYear", ci.BookYear);
            insertCommand.Parameters.AddWithValue("@BookMonth", ci.BookMonth);
            insertCommand.Parameters.AddWithValue("@BookDay", ci.BookDay);
            insertCommand.Parameters.AddWithValue("@BookCourt", ci.CourtNumber);
            insertCommand.Parameters.AddWithValue("@BookTime", ci.CourtTime);
            insertCommand.Parameters.AddWithValue("@BookDate", ci.BookDate);
            insertCommand.Parameters.AddWithValue("@BookedDate", DateTime.Now);
            insertCommand.Parameters.AddWithValue("@Status", ci.Status);

            try
            {
                connection.Open();

                int insertitem = insertCommand.ExecuteNonQuery();
                if (insertitem > 0)
                    return true;
                else
                    return false;

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                return false;
            }
            finally
            {
                connection.Close();
            }
        }
     
        public static bool AddNewBooking(CourtInformation ci)
        {
            SqlConnection connection = ConnectDB.GetConnection();
           
            SqlCommand insertCommand = new SqlCommand();
            string insertstatement = "INSERT INTO tblBookings " +
                            "(BookYear, BookMonth, BookDay, CourtNumber, CourtTime, UserEmail, Status) " +
                            "VALUES (@BookYear, @BookMonth, @BookDay, @CourtNumber, @CourtTime, @UserEmail, @Status)";

            insertCommand = new SqlCommand(insertstatement, connection);

            insertCommand.Parameters.AddWithValue("@BookYear", ci.BookYear);
            insertCommand.Parameters.AddWithValue("@BookMonth", ci.BookMonth);
            insertCommand.Parameters.AddWithValue("@BookDay", ci.BookDay);
            insertCommand.Parameters.AddWithValue("@CourtNumber", ci.CourtNumber);
            insertCommand.Parameters.AddWithValue("@CourtTime", ci.CourtTime);
            insertCommand.Parameters.AddWithValue("@UserEmail", ci.UserEmail);
            insertCommand.Parameters.AddWithValue("@Status", ci.Status);

            try
            {
                connection.Open();

                int insertcount = insertCommand.ExecuteNonQuery();
                if (insertcount > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                return false;
            }
            finally
            {
                connection.Close();
            }
        }       

        public static bool UpdateBookedCourt(CourtInformation toupdatecourt, string logged_email)
        {
            SqlConnection connection = ConnectDB.GetConnection();
            SqlCommand updateCommand = new SqlCommand();

            DateTime canceldate = Convert.ToDateTime(toupdatecourt.BookDate);
            int bookyear = canceldate.Year;
            int bookmonth = canceldate.Month;
            int bookday = canceldate.Day;
            int bookcourt = Convert.ToInt16(toupdatecourt.CourtNumber);
            int booktime = Convert.ToInt16(toupdatecourt.CourtTime);
            int status = Convert.ToInt16(toupdatecourt.Status);

            if (status == 1)
            {
                string updatestatement = "UPDATE tblBookings SET " +
                             "Status = @Status " +
                             "WHERE BookYear = @BookYear AND " +
                             "BookMonth =  @BookMonth AND " +
                             "BookDay = @BookDay AND " +
                             "CourtNumber = @CourtNumber AND " +
                             "CourtTime = @CourtTime";

                updateCommand = new SqlCommand(updatestatement, connection);

                updateCommand.Parameters.AddWithValue("@BookYear", bookyear);
                updateCommand.Parameters.AddWithValue("@BookMonth", bookmonth);
                updateCommand.Parameters.AddWithValue("@BookDay", bookday);
                updateCommand.Parameters.AddWithValue("@CourtNumber", bookcourt);
                updateCommand.Parameters.AddWithValue("@CourtTime", booktime);
                updateCommand.Parameters.AddWithValue("@Status", status);        //1- paid,  2- canceled
            }
            else
            {
                string updatestatement = "DELETE FROM tblBookings " +
                             "WHERE BookYear = @BookYear AND " +
                             "BookMonth =  @BookMonth AND " +
                             "BookDay = @BookDay AND " +
                             "CourtNumber = @CourtNumber AND " +
                             "CourtTime = @CourtTime";

                updateCommand = new SqlCommand(updatestatement, connection);

                updateCommand.Parameters.AddWithValue("@BookYear", bookyear);
                updateCommand.Parameters.AddWithValue("@BookMonth", bookmonth);
                updateCommand.Parameters.AddWithValue("@BookDay", bookday);
                updateCommand.Parameters.AddWithValue("@CourtNumber", bookcourt);
                updateCommand.Parameters.AddWithValue("@CourtTime", booktime);
            }


            try
            {
                connection.Open();

                int updatecount = updateCommand.ExecuteNonQuery();

                if (updatecount > 0)
                {
                //Update booked court in user table
                string updatestatement = "UPDATE tblUserBookedCourts SET " +
                                             "Status = @Status " +
                                             "WHERE UserEmail = @UserEmail AND BookYear = @BookYear AND " +
                                             "BookMonth =  @BookMonth AND BookDay = @BookDay AND " +
                                             "BookCourt = @CourtNumber AND BookTime = @BookTime";

                    updateCommand = new SqlCommand(updatestatement, connection);

                    updateCommand.Parameters.AddWithValue("@UserEmail", logged_email);
                    updateCommand.Parameters.AddWithValue("@BookYear", bookyear);
                    updateCommand.Parameters.AddWithValue("@BookMonth", bookmonth);
                    updateCommand.Parameters.AddWithValue("@BookDay", bookday);
                    updateCommand.Parameters.AddWithValue("@CourtNumber", bookcourt);
                    updateCommand.Parameters.AddWithValue("@BookTime", booktime);
                    updateCommand.Parameters.AddWithValue("@Status", status);

                    try
                    {
                        updateCommand.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                        return false;
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return true;
                }
                else
                    return false;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                return false;
            }
            finally
            {
                connection.Close();
            }
        }

        //public static bool CancelBookedCourt1(CourtInformation tocancelcourt, string logged_email)
        //{
        //    SqlConnection connection = ConnectDB.GetConnection();

        //    DateTime canceldate = Convert.ToDateTime(tocancelcourt.BookDate);
        //    int bookyear = canceldate.Year;
        //    int bookmonth = canceldate.Month;
        //    int bookday = canceldate.Day;
        //    int bookcourt = Convert.ToInt16(tocancelcourt.CourtNumber);
        //    int booktime = Convert.ToInt16(tocancelcourt.CourtTime);

        //    string deletestatement = "DELETE FROM tblBookings " +
        //                 "WHERE BookYear = @BookYear AND " +
        //                 "BookMonth =  @BookMonth AND " +
        //                 "BookDay = @BookDay AND " +
        //                 "CourtNumber = @CourtNumber AND " + 
        //                 "CourtTime = @CourtTime";

        //    SqlCommand deleteCommand = new SqlCommand(deletestatement, connection);

        //    deleteCommand.Parameters.AddWithValue("@BookYear", bookyear);
        //    deleteCommand.Parameters.AddWithValue("@BookMonth", bookmonth);
        //    deleteCommand.Parameters.AddWithValue("@BookDay", bookday);
        //    deleteCommand.Parameters.AddWithValue("@CourtNumber", bookcourt);
        //    deleteCommand.Parameters.AddWithValue("@CourtTime", booktime);

        //    try
        //    {
        //        connection.Open();

        //        int deletecount = deleteCommand.ExecuteNonQuery();

        //        if (deletecount > 0)
        //        {
        //            //Delete booked court in user table
        //            deletestatement = "DELETE FROM tblUserBookedCourts " +
        //                                     "WHERE UserEmail = @UserEmail AND BookYear = @BookYear AND " +
        //                                     "BookMonth =  @BookMonth AND BookDay = @BookDay AND " +
        //                                     "BookCourt = @CourtNumber AND BookTime = @BookTime";

        //            deleteCommand = new SqlCommand(deletestatement, connection);

        //            deleteCommand.Parameters.AddWithValue("@UserEmail", logged_email);
        //            deleteCommand.Parameters.AddWithValue("@BookYear", bookyear);
        //            deleteCommand.Parameters.AddWithValue("@BookMonth", bookmonth);
        //            deleteCommand.Parameters.AddWithValue("@BookDay", bookday);
        //            deleteCommand.Parameters.AddWithValue("@CourtNumber", bookcourt);
        //            deleteCommand.Parameters.AddWithValue("@BookTime", booktime);

        //            try
        //            {
        //                deleteCommand.ExecuteNonQuery();
        //            }
        //            catch (SqlException ex)
        //            {
        //                MessageBox.Show(ex.Message, ex.GetType().ToString());
        //                return false;
        //            }
        //            finally
        //            {
        //                connection.Close();
        //            }
        //            return true;
        //        }
        //        else
        //            return false;
        //    }
        //    catch (SqlException ex)
        //    {
        //        MessageBox.Show(ex.Message, ex.GetType().ToString());
        //        return false;
        //    }
        //    finally
        //    {
        //        connection.Close();
        //    }
        //}

        public static List<CourtInformation> GetUserBooking(string logged_email)
        {

            SqlConnection connection = ConnectDB.GetConnection();
            string selectstatement = "Select *  FROM tblUserBookedCourts " +
                                    "WHERE UserEmail = @UserEmail AND Status < @Status";

            SqlCommand selectCommand = new SqlCommand(selectstatement, connection);

            //selectCommand.Parameters["@UserEmail"].Value = logged_email;
            selectCommand.Parameters.AddWithValue("@UserEmail", logged_email);
            selectCommand.Parameters.AddWithValue("@Status", 2);

            try
            {
                connection.Open();

                SqlDataReader reader = selectCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    List<CourtInformation> CourtInformation = new List<CourtInformation>();
                    while (reader.Read())
                    {
                        CourtInformation ci = new CourtInformation();
                        ci.BookDate = Convert.ToDateTime(reader["BookDate"]);
                        ci.CourtTime = Convert.ToInt16(reader["BookTime"]);
                        ci.CourtNumber = Convert.ToInt16(reader["BookCourt"]);
                        ci.Status = Convert.ToInt16(reader["Status"]);

                        CourtInformation.Add(ci);
                    }
                    reader.Close();
                    return CourtInformation;
                }
                else
                {
                    return null;
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                return null;
            }
            finally
            {
                connection.Close();
            }

        }

        public static bool CheckCourtAvailability(CourtInformation ca)
        {
            SqlConnection connection = ConnectDB.GetConnection();

            SqlCommand selectCommand = new SqlCommand();
            string selectstatement = "Select * FROM tblBookings WHERE " +
                                     "BookYear = @BookYear AND " +
                                     "BookMonth = @BookMonth AND " +
                                     "BookDay = @BookDay AND " +
                                     "CourtNumber = @CourtNumber AND " +
                                     "CourtTime = @CourtTime";

            selectCommand = new SqlCommand(selectstatement, connection);

            selectCommand.Parameters.AddWithValue("@BookYear", ca.BookYear);
            selectCommand.Parameters.AddWithValue("@BookMonth", ca.BookMonth);
            selectCommand.Parameters.AddWithValue("@BookDay", ca.BookDay);
            selectCommand.Parameters.AddWithValue("@CourtNumber", ca.CourtNumber);
            selectCommand.Parameters.AddWithValue("@CourtTime", ca.CourtTime);

            try
            {
                connection.Open();

                SqlDataReader reader = selectCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    MessageBox.Show("Court is no longer available.", "Book a court");
                    //reader.Close();     //close reader   
                    return false;
                }
                else
                {
                    //reader.Close();     //close reader   
                    return true;        //court is available
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
                return false;
            }
            finally
            {
                connection.Close();
            }
        }
    }

}

