﻿namespace CourtBooking
{
    partial class frmViewCancelBookedCourt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.tlpBookedCourt = new System.Windows.Forms.TableLayoutPanel();
            this.txtDate2 = new System.Windows.Forms.TextBox();
            this.txtDate1 = new System.Windows.Forms.TextBox();
            this.txtTime1 = new System.Windows.Forms.TextBox();
            this.txtCourt1 = new System.Windows.Forms.TextBox();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.txtCourt = new System.Windows.Forms.TextBox();
            this.txtCourt2 = new System.Windows.Forms.TextBox();
            this.txtTime2 = new System.Windows.Forms.TextBox();
            this.txtDate3 = new System.Windows.Forms.TextBox();
            this.txtTime3 = new System.Windows.Forms.TextBox();
            this.txtCourt3 = new System.Windows.Forms.TextBox();
            this.txtDate4 = new System.Windows.Forms.TextBox();
            this.txtDate5 = new System.Windows.Forms.TextBox();
            this.txtDate6 = new System.Windows.Forms.TextBox();
            this.txtTime4 = new System.Windows.Forms.TextBox();
            this.txtTime5 = new System.Windows.Forms.TextBox();
            this.txtTime6 = new System.Windows.Forms.TextBox();
            this.txtCourt4 = new System.Windows.Forms.TextBox();
            this.txtCourt5 = new System.Windows.Forms.TextBox();
            this.txtCourt6 = new System.Windows.Forms.TextBox();
            this.btnCancel1 = new System.Windows.Forms.Button();
            this.btnCancel2 = new System.Windows.Forms.Button();
            this.btnCancel3 = new System.Windows.Forms.Button();
            this.btnCancel4 = new System.Windows.Forms.Button();
            this.btnCancel5 = new System.Windows.Forms.Button();
            this.btnCancel6 = new System.Windows.Forms.Button();
            this.btnPay1 = new System.Windows.Forms.Button();
            this.btnPay2 = new System.Windows.Forms.Button();
            this.btnPay3 = new System.Windows.Forms.Button();
            this.btnPay4 = new System.Windows.Forms.Button();
            this.btnPay5 = new System.Windows.Forms.Button();
            this.btnPay6 = new System.Windows.Forms.Button();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.dgvUserBookedCourts = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tlpBookedCourt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserBookedCourts)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Your booked court(s):";
            // 
            // tlpBookedCourt
            // 
            this.tlpBookedCourt.AutoSize = true;
            this.tlpBookedCourt.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tlpBookedCourt.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tlpBookedCourt.ColumnCount = 5;
            this.tlpBookedCourt.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 176F));
            this.tlpBookedCourt.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tlpBookedCourt.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tlpBookedCourt.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.tlpBookedCourt.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tlpBookedCourt.Controls.Add(this.txtDate2, 0, 2);
            this.tlpBookedCourt.Controls.Add(this.txtDate1, 0, 1);
            this.tlpBookedCourt.Controls.Add(this.txtTime1, 1, 1);
            this.tlpBookedCourt.Controls.Add(this.txtCourt1, 2, 1);
            this.tlpBookedCourt.Controls.Add(this.txtTime, 1, 0);
            this.tlpBookedCourt.Controls.Add(this.txtCourt, 2, 0);
            this.tlpBookedCourt.Controls.Add(this.txtCourt2, 2, 2);
            this.tlpBookedCourt.Controls.Add(this.txtTime2, 1, 2);
            this.tlpBookedCourt.Controls.Add(this.txtDate3, 0, 3);
            this.tlpBookedCourt.Controls.Add(this.txtTime3, 1, 3);
            this.tlpBookedCourt.Controls.Add(this.txtCourt3, 2, 3);
            this.tlpBookedCourt.Controls.Add(this.txtDate4, 0, 4);
            this.tlpBookedCourt.Controls.Add(this.txtDate5, 0, 5);
            this.tlpBookedCourt.Controls.Add(this.txtDate6, 0, 6);
            this.tlpBookedCourt.Controls.Add(this.txtTime4, 1, 4);
            this.tlpBookedCourt.Controls.Add(this.txtTime5, 1, 5);
            this.tlpBookedCourt.Controls.Add(this.txtTime6, 1, 6);
            this.tlpBookedCourt.Controls.Add(this.txtCourt4, 2, 4);
            this.tlpBookedCourt.Controls.Add(this.txtCourt5, 2, 5);
            this.tlpBookedCourt.Controls.Add(this.txtCourt6, 2, 6);
            this.tlpBookedCourt.Controls.Add(this.btnCancel1, 4, 1);
            this.tlpBookedCourt.Controls.Add(this.btnCancel2, 4, 2);
            this.tlpBookedCourt.Controls.Add(this.btnCancel3, 4, 3);
            this.tlpBookedCourt.Controls.Add(this.btnCancel4, 4, 4);
            this.tlpBookedCourt.Controls.Add(this.btnCancel5, 4, 5);
            this.tlpBookedCourt.Controls.Add(this.btnCancel6, 4, 6);
            this.tlpBookedCourt.Controls.Add(this.btnPay1, 3, 1);
            this.tlpBookedCourt.Controls.Add(this.btnPay2, 3, 2);
            this.tlpBookedCourt.Controls.Add(this.btnPay3, 3, 3);
            this.tlpBookedCourt.Controls.Add(this.btnPay4, 3, 4);
            this.tlpBookedCourt.Controls.Add(this.btnPay5, 3, 5);
            this.tlpBookedCourt.Controls.Add(this.btnPay6, 3, 6);
            this.tlpBookedCourt.Controls.Add(this.txtDate, 0, 0);
            this.tlpBookedCourt.Location = new System.Drawing.Point(26, 279);
            this.tlpBookedCourt.Name = "tlpBookedCourt";
            this.tlpBookedCourt.RowCount = 7;
            this.tlpBookedCourt.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpBookedCourt.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpBookedCourt.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpBookedCourt.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpBookedCourt.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpBookedCourt.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpBookedCourt.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpBookedCourt.Size = new System.Drawing.Size(433, 198);
            this.tlpBookedCourt.TabIndex = 6;
            // 
            // txtDate2
            // 
            this.txtDate2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDate2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDate2.Location = new System.Drawing.Point(4, 57);
            this.txtDate2.Name = "txtDate2";
            this.txtDate2.Size = new System.Drawing.Size(170, 13);
            this.txtDate2.TabIndex = 12;
            this.txtDate2.Text = " ";
            this.txtDate2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDate1
            // 
            this.txtDate1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDate1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDate1.Location = new System.Drawing.Point(4, 30);
            this.txtDate1.Name = "txtDate1";
            this.txtDate1.Size = new System.Drawing.Size(170, 13);
            this.txtDate1.TabIndex = 4;
            this.txtDate1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTime1
            // 
            this.txtTime1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTime1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTime1.Location = new System.Drawing.Point(181, 30);
            this.txtTime1.Name = "txtTime1";
            this.txtTime1.Size = new System.Drawing.Size(60, 13);
            this.txtTime1.TabIndex = 5;
            this.txtTime1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCourt1
            // 
            this.txtCourt1.AcceptsReturn = true;
            this.txtCourt1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCourt1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCourt1.Location = new System.Drawing.Point(248, 30);
            this.txtCourt1.Name = "txtCourt1";
            this.txtCourt1.Size = new System.Drawing.Size(61, 13);
            this.txtCourt1.TabIndex = 6;
            this.txtCourt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTime
            // 
            this.txtTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTime.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTime.Location = new System.Drawing.Point(181, 4);
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(60, 16);
            this.txtTime.TabIndex = 1;
            this.txtTime.Text = "Time";
            this.txtTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCourt
            // 
            this.txtCourt.AcceptsReturn = true;
            this.txtCourt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCourt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtCourt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCourt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCourt.Location = new System.Drawing.Point(248, 4);
            this.txtCourt.Name = "txtCourt";
            this.txtCourt.Size = new System.Drawing.Size(61, 16);
            this.txtCourt.TabIndex = 2;
            this.txtCourt.Text = "Court No";
            this.txtCourt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCourt2
            // 
            this.txtCourt2.AcceptsReturn = true;
            this.txtCourt2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCourt2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCourt2.Location = new System.Drawing.Point(248, 57);
            this.txtCourt2.Name = "txtCourt2";
            this.txtCourt2.Size = new System.Drawing.Size(61, 13);
            this.txtCourt2.TabIndex = 10;
            this.txtCourt2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTime2
            // 
            this.txtTime2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTime2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTime2.Location = new System.Drawing.Point(181, 57);
            this.txtTime2.Name = "txtTime2";
            this.txtTime2.Size = new System.Drawing.Size(60, 13);
            this.txtTime2.TabIndex = 9;
            this.txtTime2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDate3
            // 
            this.txtDate3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDate3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDate3.Location = new System.Drawing.Point(4, 86);
            this.txtDate3.Name = "txtDate3";
            this.txtDate3.Size = new System.Drawing.Size(170, 13);
            this.txtDate3.TabIndex = 13;
            this.txtDate3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTime3
            // 
            this.txtTime3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTime3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTime3.Location = new System.Drawing.Point(181, 86);
            this.txtTime3.Name = "txtTime3";
            this.txtTime3.Size = new System.Drawing.Size(60, 13);
            this.txtTime3.TabIndex = 14;
            this.txtTime3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCourt3
            // 
            this.txtCourt3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCourt3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCourt3.Location = new System.Drawing.Point(248, 86);
            this.txtCourt3.Name = "txtCourt3";
            this.txtCourt3.Size = new System.Drawing.Size(61, 13);
            this.txtCourt3.TabIndex = 15;
            this.txtCourt3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDate4
            // 
            this.txtDate4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDate4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDate4.Location = new System.Drawing.Point(4, 116);
            this.txtDate4.Name = "txtDate4";
            this.txtDate4.Size = new System.Drawing.Size(170, 13);
            this.txtDate4.TabIndex = 16;
            this.txtDate4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDate5
            // 
            this.txtDate5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDate5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDate5.Location = new System.Drawing.Point(4, 146);
            this.txtDate5.Name = "txtDate5";
            this.txtDate5.Size = new System.Drawing.Size(170, 13);
            this.txtDate5.TabIndex = 17;
            this.txtDate5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDate6
            // 
            this.txtDate6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDate6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDate6.Location = new System.Drawing.Point(4, 176);
            this.txtDate6.Name = "txtDate6";
            this.txtDate6.Size = new System.Drawing.Size(170, 13);
            this.txtDate6.TabIndex = 18;
            this.txtDate6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTime4
            // 
            this.txtTime4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTime4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTime4.Location = new System.Drawing.Point(181, 116);
            this.txtTime4.Name = "txtTime4";
            this.txtTime4.Size = new System.Drawing.Size(60, 13);
            this.txtTime4.TabIndex = 19;
            this.txtTime4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTime5
            // 
            this.txtTime5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTime5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTime5.Location = new System.Drawing.Point(181, 146);
            this.txtTime5.Name = "txtTime5";
            this.txtTime5.Size = new System.Drawing.Size(60, 13);
            this.txtTime5.TabIndex = 20;
            this.txtTime5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTime6
            // 
            this.txtTime6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTime6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTime6.Location = new System.Drawing.Point(181, 176);
            this.txtTime6.Name = "txtTime6";
            this.txtTime6.Size = new System.Drawing.Size(60, 13);
            this.txtTime6.TabIndex = 21;
            this.txtTime6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCourt4
            // 
            this.txtCourt4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCourt4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCourt4.Location = new System.Drawing.Point(248, 116);
            this.txtCourt4.Name = "txtCourt4";
            this.txtCourt4.Size = new System.Drawing.Size(61, 13);
            this.txtCourt4.TabIndex = 22;
            this.txtCourt4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCourt5
            // 
            this.txtCourt5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCourt5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCourt5.Location = new System.Drawing.Point(248, 146);
            this.txtCourt5.Name = "txtCourt5";
            this.txtCourt5.Size = new System.Drawing.Size(61, 13);
            this.txtCourt5.TabIndex = 23;
            this.txtCourt5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCourt6
            // 
            this.txtCourt6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCourt6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCourt6.Location = new System.Drawing.Point(248, 176);
            this.txtCourt6.Name = "txtCourt6";
            this.txtCourt6.Size = new System.Drawing.Size(61, 13);
            this.txtCourt6.TabIndex = 24;
            this.txtCourt6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnCancel1
            // 
            this.btnCancel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel1.Location = new System.Drawing.Point(375, 27);
            this.btnCancel1.Name = "btnCancel1";
            this.btnCancel1.Size = new System.Drawing.Size(54, 20);
            this.btnCancel1.TabIndex = 7;
            this.btnCancel1.Text = "Cancel";
            this.btnCancel1.UseVisualStyleBackColor = true;
            this.btnCancel1.Click += new System.EventHandler(this.btnCancel1_Click);
            // 
            // btnCancel2
            // 
            this.btnCancel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel2.Location = new System.Drawing.Point(375, 54);
            this.btnCancel2.Name = "btnCancel2";
            this.btnCancel2.Size = new System.Drawing.Size(54, 20);
            this.btnCancel2.TabIndex = 11;
            this.btnCancel2.Text = "Cancel";
            this.btnCancel2.UseVisualStyleBackColor = true;
            this.btnCancel2.Click += new System.EventHandler(this.btnCancel2_Click);
            // 
            // btnCancel3
            // 
            this.btnCancel3.Location = new System.Drawing.Point(375, 81);
            this.btnCancel3.Name = "btnCancel3";
            this.btnCancel3.Size = new System.Drawing.Size(49, 23);
            this.btnCancel3.TabIndex = 25;
            this.btnCancel3.Text = "Cancel";
            this.btnCancel3.UseVisualStyleBackColor = true;
            this.btnCancel3.Click += new System.EventHandler(this.btnCancel3_Click);
            // 
            // btnCancel4
            // 
            this.btnCancel4.Location = new System.Drawing.Point(375, 111);
            this.btnCancel4.Name = "btnCancel4";
            this.btnCancel4.Size = new System.Drawing.Size(49, 23);
            this.btnCancel4.TabIndex = 26;
            this.btnCancel4.Text = "Cancel";
            this.btnCancel4.UseVisualStyleBackColor = true;
            this.btnCancel4.Click += new System.EventHandler(this.btnCancel4_Click);
            // 
            // btnCancel5
            // 
            this.btnCancel5.Location = new System.Drawing.Point(375, 141);
            this.btnCancel5.Name = "btnCancel5";
            this.btnCancel5.Size = new System.Drawing.Size(49, 23);
            this.btnCancel5.TabIndex = 27;
            this.btnCancel5.Text = "Cancel";
            this.btnCancel5.UseVisualStyleBackColor = true;
            this.btnCancel5.Click += new System.EventHandler(this.btnCancel5_Click);
            // 
            // btnCancel6
            // 
            this.btnCancel6.Location = new System.Drawing.Point(375, 171);
            this.btnCancel6.Name = "btnCancel6";
            this.btnCancel6.Size = new System.Drawing.Size(49, 23);
            this.btnCancel6.TabIndex = 28;
            this.btnCancel6.Text = "Cancel";
            this.btnCancel6.UseVisualStyleBackColor = true;
            this.btnCancel6.Click += new System.EventHandler(this.btnCancel6_Click);
            // 
            // btnPay1
            // 
            this.btnPay1.Location = new System.Drawing.Point(316, 27);
            this.btnPay1.Name = "btnPay1";
            this.btnPay1.Size = new System.Drawing.Size(52, 20);
            this.btnPay1.TabIndex = 29;
            this.btnPay1.Text = "Pay";
            this.btnPay1.UseVisualStyleBackColor = true;
            this.btnPay1.Click += new System.EventHandler(this.PayButton_Click);
            // 
            // btnPay2
            // 
            this.btnPay2.Location = new System.Drawing.Point(316, 54);
            this.btnPay2.Name = "btnPay2";
            this.btnPay2.Size = new System.Drawing.Size(52, 20);
            this.btnPay2.TabIndex = 30;
            this.btnPay2.Text = "Pay";
            this.btnPay2.UseVisualStyleBackColor = true;
            this.btnPay2.Click += new System.EventHandler(this.PayButton_Click);
            // 
            // btnPay3
            // 
            this.btnPay3.Location = new System.Drawing.Point(316, 81);
            this.btnPay3.Name = "btnPay3";
            this.btnPay3.Size = new System.Drawing.Size(52, 20);
            this.btnPay3.TabIndex = 31;
            this.btnPay3.Text = "Pay";
            this.btnPay3.UseVisualStyleBackColor = true;
            this.btnPay3.Click += new System.EventHandler(this.PayButton_Click);
            // 
            // btnPay4
            // 
            this.btnPay4.Location = new System.Drawing.Point(316, 111);
            this.btnPay4.Name = "btnPay4";
            this.btnPay4.Size = new System.Drawing.Size(52, 20);
            this.btnPay4.TabIndex = 32;
            this.btnPay4.Text = "Pay";
            this.btnPay4.UseVisualStyleBackColor = true;
            this.btnPay4.Click += new System.EventHandler(this.PayButton_Click);
            // 
            // btnPay5
            // 
            this.btnPay5.Location = new System.Drawing.Point(316, 141);
            this.btnPay5.Name = "btnPay5";
            this.btnPay5.Size = new System.Drawing.Size(52, 20);
            this.btnPay5.TabIndex = 33;
            this.btnPay5.Text = "Pay";
            this.btnPay5.UseVisualStyleBackColor = true;
            this.btnPay5.Click += new System.EventHandler(this.PayButton_Click);
            // 
            // btnPay6
            // 
            this.btnPay6.Location = new System.Drawing.Point(316, 171);
            this.btnPay6.Name = "btnPay6";
            this.btnPay6.Size = new System.Drawing.Size(52, 20);
            this.btnPay6.TabIndex = 34;
            this.btnPay6.Text = "Pay";
            this.btnPay6.UseVisualStyleBackColor = true;
            this.btnPay6.Click += new System.EventHandler(this.PayButton_Click);
            // 
            // txtDate
            // 
            this.txtDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDate.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtDate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDate.Location = new System.Drawing.Point(4, 4);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(170, 16);
            this.txtDate.TabIndex = 0;
            this.txtDate.Text = "Date";
            this.txtDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dgvUserBookedCourts
            // 
            this.dgvUserBookedCourts.AllowUserToResizeColumns = false;
            this.dgvUserBookedCourts.AllowUserToResizeRows = false;
            this.dgvUserBookedCourts.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUserBookedCourts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvUserBookedCourts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUserBookedCourts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dgvUserBookedCourts.Location = new System.Drawing.Point(26, 61);
            this.dgvUserBookedCourts.Name = "dgvUserBookedCourts";
            this.dgvUserBookedCourts.RowHeadersVisible = false;
            this.dgvUserBookedCourts.Size = new System.Drawing.Size(443, 45);
            this.dgvUserBookedCourts.TabIndex = 7;
            // 
            // Column1
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column1.HeaderText = "Date";
            this.Column1.Name = "Column1";
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 180;
            // 
            // Column2
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Blue;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column2.HeaderText = "Time";
            this.Column2.Name = "Column2";
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 70;
            // 
            // Column3
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Blue;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column3.HeaderText = "Court ";
            this.Column3.Name = "Column3";
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column3.Width = 70;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "";
            this.Column4.Name = "Column4";
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column4.Width = 60;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "";
            this.Column5.Name = "Column5";
            this.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column5.Width = 60;
            // 
            // frmViewCancelBookedCourt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 268);
            this.Controls.Add(this.dgvUserBookedCourts);
            this.Controls.Add(this.tlpBookedCourt);
            this.Controls.Add(this.label1);
            this.Name = "frmViewCancelBookedCourt";
            this.Text = "Booked Court";
            this.Load += new System.EventHandler(this.frmViewCancelBookedCourt_Load);
            this.tlpBookedCourt.ResumeLayout(false);
            this.tlpBookedCourt.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserBookedCourts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tlpBookedCourt;
        private System.Windows.Forms.TextBox txtTime;
        private System.Windows.Forms.TextBox txtCourt;
        private System.Windows.Forms.TextBox txtDate1;
        private System.Windows.Forms.TextBox txtTime1;
        private System.Windows.Forms.TextBox txtCourt1;
        private System.Windows.Forms.Button btnCancel1;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.TextBox txtDate2;
        private System.Windows.Forms.Button btnCancel2;
        private System.Windows.Forms.TextBox txtCourt2;
        private System.Windows.Forms.TextBox txtTime2;
        private System.Windows.Forms.TextBox txtDate3;
        private System.Windows.Forms.TextBox txtTime3;
        private System.Windows.Forms.TextBox txtCourt3;
        private System.Windows.Forms.TextBox txtDate4;
        private System.Windows.Forms.TextBox txtDate5;
        private System.Windows.Forms.TextBox txtDate6;
        private System.Windows.Forms.TextBox txtTime4;
        private System.Windows.Forms.TextBox txtTime5;
        private System.Windows.Forms.TextBox txtTime6;
        private System.Windows.Forms.TextBox txtCourt4;
        private System.Windows.Forms.TextBox txtCourt5;
        private System.Windows.Forms.TextBox txtCourt6;
        private System.Windows.Forms.Button btnCancel3;
        private System.Windows.Forms.Button btnCancel4;
        private System.Windows.Forms.Button btnCancel5;
        private System.Windows.Forms.Button btnCancel6;
        private System.Windows.Forms.Button btnPay1;
        private System.Windows.Forms.Button btnPay2;
        private System.Windows.Forms.Button btnPay3;
        private System.Windows.Forms.Button btnPay4;
        private System.Windows.Forms.Button btnPay5;
        private System.Windows.Forms.Button btnPay6;
        private System.Windows.Forms.DataGridView dgvUserBookedCourts;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
    }
}