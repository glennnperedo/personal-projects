﻿namespace CourtBooking
{
    partial class frmBookingPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvCourtBooking = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDate1 = new System.Windows.Forms.Button();
            this.btnDate2 = new System.Windows.Forms.Button();
            this.btnDate3 = new System.Windows.Forms.Button();
            this.btnDate4 = new System.Windows.Forms.Button();
            this.btnDate5 = new System.Windows.Forms.Button();
            this.btnDate6 = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCourtBooking)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvCourtBooking
            // 
            this.dgvCourtBooking.AllowUserToAddRows = false;
            this.dgvCourtBooking.AllowUserToDeleteRows = false;
            this.dgvCourtBooking.AllowUserToResizeColumns = false;
            this.dgvCourtBooking.AllowUserToResizeRows = false;
            this.dgvCourtBooking.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvCourtBooking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCourtBooking.Location = new System.Drawing.Point(32, 93);
            this.dgvCourtBooking.Name = "dgvCourtBooking";
            this.dgvCourtBooking.ReadOnly = true;
            this.dgvCourtBooking.Size = new System.Drawing.Size(822, 628);
            this.dgvCourtBooking.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Book a Court";
            // 
            // btnDate1
            // 
            this.btnDate1.BackColor = System.Drawing.Color.Transparent;
            this.btnDate1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnDate1.ForeColor = System.Drawing.Color.Black;
            this.btnDate1.Location = new System.Drawing.Point(55, 64);
            this.btnDate1.Name = "btnDate1";
            this.btnDate1.Size = new System.Drawing.Size(125, 23);
            this.btnDate1.TabIndex = 2;
            this.btnDate1.UseVisualStyleBackColor = false;
            this.btnDate1.Click += new System.EventHandler(this.btnDate1_Click);
            // 
            // btnDate2
            // 
            this.btnDate2.BackColor = System.Drawing.Color.Transparent;
            this.btnDate2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnDate2.ForeColor = System.Drawing.Color.Black;
            this.btnDate2.Location = new System.Drawing.Point(186, 64);
            this.btnDate2.Name = "btnDate2";
            this.btnDate2.Size = new System.Drawing.Size(125, 23);
            this.btnDate2.TabIndex = 3;
            this.btnDate2.UseVisualStyleBackColor = false;
            this.btnDate2.Click += new System.EventHandler(this.btnDate2_Click);
            // 
            // btnDate3
            // 
            this.btnDate3.BackColor = System.Drawing.Color.Transparent;
            this.btnDate3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnDate3.ForeColor = System.Drawing.Color.Black;
            this.btnDate3.Location = new System.Drawing.Point(317, 64);
            this.btnDate3.Name = "btnDate3";
            this.btnDate3.Size = new System.Drawing.Size(125, 23);
            this.btnDate3.TabIndex = 4;
            this.btnDate3.UseVisualStyleBackColor = false;
            this.btnDate3.Click += new System.EventHandler(this.btnDate3_Click);
            // 
            // btnDate4
            // 
            this.btnDate4.BackColor = System.Drawing.Color.Transparent;
            this.btnDate4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnDate4.ForeColor = System.Drawing.Color.Black;
            this.btnDate4.Location = new System.Drawing.Point(448, 64);
            this.btnDate4.Name = "btnDate4";
            this.btnDate4.Size = new System.Drawing.Size(125, 23);
            this.btnDate4.TabIndex = 5;
            this.btnDate4.UseVisualStyleBackColor = false;
            this.btnDate4.Click += new System.EventHandler(this.btnDate4_Click);
            // 
            // btnDate5
            // 
            this.btnDate5.BackColor = System.Drawing.Color.Transparent;
            this.btnDate5.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnDate5.ForeColor = System.Drawing.Color.Black;
            this.btnDate5.Location = new System.Drawing.Point(579, 64);
            this.btnDate5.Name = "btnDate5";
            this.btnDate5.Size = new System.Drawing.Size(125, 23);
            this.btnDate5.TabIndex = 6;
            this.btnDate5.UseVisualStyleBackColor = false;
            this.btnDate5.Click += new System.EventHandler(this.btnDate5_Click);
            // 
            // btnDate6
            // 
            this.btnDate6.BackColor = System.Drawing.Color.Transparent;
            this.btnDate6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnDate6.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnDate6.ForeColor = System.Drawing.Color.Black;
            this.btnDate6.Location = new System.Drawing.Point(710, 64);
            this.btnDate6.Name = "btnDate6";
            this.btnDate6.Size = new System.Drawing.Size(125, 23);
            this.btnDate6.TabIndex = 7;
            this.btnDate6.UseVisualStyleBackColor = false;
            this.btnDate6.Click += new System.EventHandler(this.btnDate6_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(183, 28);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 8;
            this.lblName.Text = "label2";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(766, 28);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(69, 23);
            this.btnRefresh.TabIndex = 9;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // frmBookingPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(889, 742);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.btnDate6);
            this.Controls.Add(this.btnDate5);
            this.Controls.Add(this.btnDate4);
            this.Controls.Add(this.btnDate3);
            this.Controls.Add(this.btnDate2);
            this.Controls.Add(this.btnDate1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvCourtBooking);
            this.Name = "frmBookingPage";
            this.Text = "frmBookingInformation";
            this.Load += new System.EventHandler(this.frmBookingPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCourtBooking)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCourtBooking;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDate1;
        private System.Windows.Forms.Button btnDate2;
        private System.Windows.Forms.Button btnDate3;
        private System.Windows.Forms.Button btnDate4;
        private System.Windows.Forms.Button btnDate5;
        private System.Windows.Forms.Button btnDate6;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Button btnRefresh;
    }
}