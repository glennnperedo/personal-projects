﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourtBooking
{
    public partial class frmMain : Form
    {
        private string logged_email;
        private string logged_firstname;
        private string logged_lastname;
        private string logged_password;

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            DateTime dateToday = DateTime.Today;
            lblDate.Text = "Today is " + dateToday.ToString("ddd") + ", " + dateToday.ToString("MMM dd, yyyy");
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            frmRegistration registerForm = new frmRegistration();
            DialogResult registrationresult = registerForm.ShowDialog();

            if (registrationresult == DialogResult.OK)
                MessageBox.Show("You may now login with your email and password.", "Login");
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (btnLogin.Text == "Login")
            {
                UserDB.Email = txtEmail.Text;
                UserDB.Password = txtPassword.Text;

                if (!UserDB.Login())
                {
                    MessageBox.Show("Invalid email and/or password entered.", "User Login");
                }
                else
                {
                    //Login succeeded
                    logged_email = txtEmail.Text;
                    logged_password = txtPassword.Text;
                    logged_firstname = UserDB.FirstName;
                    logged_lastname = UserDB.LastName;

                    lblWelcome.Visible = true;
                    lblUser.Visible = true;
                    btnBookCourt.Enabled = true;
                    btnViewCancelCourt.Enabled = true;
                    btnRegister.Enabled = false;
                    btnLogin.Text = "Logout";
                    lblUser.Text = UserDB.FirstName + " " + UserDB.LastName;
                    txtEmail.Text = "";
                    txtPassword.Text = "";
                }
            }
            else
            {
                //User logout
                logged_email = "";
                logged_firstname = "";
                logged_lastname = "";
                txtEmail.Text = "";
                txtPassword.Text = "";
                lblWelcome.Visible = false;
                lblUser.Visible = false;
                btnBookCourt.Enabled = false; ;
                btnViewCancelCourt.Enabled = false;
                btnRegister.Enabled = true;
                btnLogin.Text = "Login";
                lblUser.Text = "";
            }
        }

        private void btnBookCourt_Click(object sender, EventArgs e)
        {
            //Display BookingPage form
            frmBookingPage bookingpage = new frmBookingPage();

            //Pass these info to form properties
            bookingpage.logged_email = logged_email;
            bookingpage.logged_firstname = logged_firstname;
            bookingpage.logged_lastname = logged_lastname;
            bookingpage.ShowDialog();
        }

        private void btnViewCancelCourt_Click(object sender, EventArgs e)
        {
            List<CourtInformation> userbookedcourts = new List<CourtInformation>();

            userbookedcourts = BookingsDB.GetUserBooking(logged_email);        // fetch Booked Courts for this user

            if (userbookedcourts != null)
            {
                frmViewCancelBookedCourt bookedcourts = new frmViewCancelBookedCourt();
                bookedcourts.savedCourtInformation = userbookedcourts;      // instance variable in frmViewCancelBookedCourt
                bookedcourts.user_email = logged_email;                     // instance variable in frmViewCancelBookedCourt

                bookedcourts.ShowDialog();
            }
            else
                MessageBox.Show("You have no booked court.", "Display booked court");
        }
    }
}
